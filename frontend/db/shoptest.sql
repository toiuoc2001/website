-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2024 at 05:47 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_ban_sua`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `token` varchar(50) NOT NULL,
  `verify` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `user_name`, `email`, `password`, `date_added`, `token`, `verify`) VALUES
(7, 'hung', 'svl89278@omeie.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', '2024-02-26 07:10:54', '943368', 0),
(9, 'hung', 'toiuoc2001@gmail.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', '2024-02-26 07:13:05', '212676', 1),
(10, 'hung', 'ugg15611@omeie.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', '2024-02-26 07:34:14', '574608', 0),
(11, 'hung', 'toanhq1312@gmail.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', '2024-02-26 07:38:54', '683489', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1700644721),
('m130524_201442_init', 1700644730),
('m190124_110200_add_verification_token_column_to_user_table', 1700644730);

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `id_order_detail` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id_order_detail`, `id_user`, `quantity`, `total_price`, `date_added`, `status`) VALUES
(8, 11, 8, 2288000, '2024-03-21 09:37:17', 1),
(9, 11, 9, 2288000, '2024-03-21 09:40:31', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id_order_item` int(11) NOT NULL,
  `id_order_detail` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity_order_item` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id_order_item`, `id_order_detail`, `id_product`, `quantity_order_item`, `price`) VALUES
(18, 8, 6, 2, 125000),
(19, 8, 19, 1, 120000),
(20, 8, 21, 1, 293000),
(21, 8, 10, 1, 400000),
(22, 8, 8, 1, 125000),
(23, 8, 7, 2, 500000),
(24, 9, 15, 1, 100000),
(25, 9, 6, 2, 125000),
(26, 9, 19, 1, 120000),
(27, 9, 21, 1, 293000),
(28, 9, 10, 1, 400000),
(29, 9, 8, 1, 125000),
(30, 9, 7, 2, 500000);

-- --------------------------------------------------------

--
-- Table structure for table `otp`
--

CREATE TABLE `otp` (
  `id_otp` int(11) NOT NULL,
  `user_otp` int(100) NOT NULL,
  `otp_code` varchar(45) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `otp`
--

INSERT INTO `otp` (`id_otp`, `user_otp`, `otp_code`, `time`) VALUES
(1, 0, '129214', '2024-01-05 04:48:55'),
(2, 0, '688476', '2024-01-05 04:49:45'),
(3, 9, '419812', '2024-02-26 07:55:26'),
(4, 9, '508377', '2024-02-26 07:55:28'),
(5, 9, '372649', '2024-02-26 08:04:15'),
(6, 9, '795922', '2024-02-26 08:05:01'),
(7, 9, '851405', '2024-02-26 08:08:40'),
(8, 9, '490390', '2024-02-26 08:09:35'),
(9, 9, '5463497', '2024-02-26 08:17:05'),
(10, 0, '225004', '2024-04-25 06:50:04');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id_product` int(11) NOT NULL,
  `id_product_categorys` int(11) NOT NULL DEFAULT 0,
  `product_name` varchar(100) NOT NULL,
  `product_image` varchar(500) NOT NULL,
  `product_price` int(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `product_info` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_product`, `id_product_categorys`, `product_name`, `product_image`, `product_price`, `quantity`, `date_added`, `product_info`) VALUES
(6, 1, 'pizza bò', 'Pizza+Extra+Topping+(2).jpg', 125000, 16, '2023-12-19 08:40:01', 'Cùng thưởng thức 2 loại nhân phủ thơm ngon trên cùng 1 bánh pizza. Pizza Half Half - vị ngon nhân đôi.'),
(7, 1, 'pizza megalodon', 'PC-MB1000X667px+super+topping@2x.png', 500000, 8, '2023-12-19 10:25:08', 'pizza đăc biệt'),
(8, 1, 'pizza italia', 'Pizza+Extra+Topping+(4).jpg', 125000, 10, '2023-12-19 10:25:41', 'fdadfhdfdfghfdgcv'),
(9, 1, 'pizza mozazzella', 'Pizza+Extra+Topping+(4).jpg', 125000, 90, '2023-12-20 03:18:41', 'hdsefsgdfhgjhjuiyrrdtghbjn'),
(10, 1, 'pizza piger', 'Pizza+Extra+Topping+(3).jpg', 400000, 118, '2023-12-20 03:19:17', 'oghgbnijo'),
(11, 3, 'Buger chiken', 'burger_ga_pho_mai_so_t_bbq.jpg', 50000, 100, '2024-01-15 07:04:11', 'đây là một sản phẩm đến từ buger king'),
(12, 3, 'Buger cheese', 'burger-2-mi_ng-b_-ph_-mai-thit-heo-x_ng-kh_i-s_t-bbq_1.jpg', 60000, 50, '2024-01-15 07:05:05', 'rất ngon và hấp dẫn'),
(13, 3, 'buger bò', '17-whopper-b_-t_m-ph_-mai-c_-l_n_1_1.jpg', 80000, 45, '2024-01-15 07:06:56', 'buger bò được làm từ thịt bò tươi,...'),
(14, 3, 'buger x2 thịt bò', '2-mieng-b_-burger-b_-n_ng-whopper_3.jpg', 90000, 32, '2024-01-15 07:07:55', 'rát ngon'),
(15, 3, 'buger thị giam bông, và thị bò , cheesse', '2-mieng-bo-burger-b_-ph_-mai_1.jpg', 100000, 48, '2024-01-15 07:09:02', 'rất ngon'),
(16, 1, 'pizza chicago', 'Pizza+Extra+Topping+(2).jpg', 125000, 22, '2024-01-15 07:15:43', 'rất ngon'),
(17, 3, 'buger thị xông khói', '12-burger-b_-n_ng-h_nh-chi_n_4.jpg', 30000, 44, '2024-01-15 07:32:26', 'rất ngon'),
(18, 3, 'buger chiken cheese ', '2-mieng-bo-burger-b_-ph_-mai_1.jpg', 20000, 56, '2024-01-15 07:33:53', 'rất ngon'),
(19, 1, 'pizza ngô mực', 'PIZZA+CUA+ST+(2).jpg', 120000, 36, '2024-01-15 07:52:59', 'rất ngon'),
(20, 1, 'pizza 100% italia', 'PIZZA+CUA+ST+(1).jpg', 125000, 20, '2024-01-15 07:53:46', 'rất ngon'),
(21, 1, 'PIZZA NEW YORK BÒ BEEFSTEAK PHÔ MAI', 'PC-MB1000X667px+super+topping@2x.png', 293000, 16, '2024-01-15 07:56:18', 'rất ngon'),
(22, 3, 'burger nhật bản', '17-whopper-b_-t_m-ph_-mai-c_-l_n_1_1.jpg', 40000, 20, '2024-02-08 15:14:33', 'rất ngon'),
(23, 3, 'burger king', 'burger_ga_pho_mai_so_t_bbq.jpg', 43335, 43, '2024-02-08 15:17:29', 'rất ngon'),
(24, 3, 'burger phô mai lát thị bò', 'cheese-ring-beef-burger.jpg', 100000, 24, '2024-02-08 15:18:57', 'rất ngon'),
(25, 1, 'PIZZA SINGAPORE CUA XỐT TIÊU ĐEN MAYO - SINGAPORE MAYO BLACK PEPPER CRAB', 'Pizza+Extra+Topping+(4).jpg', 230000, 8, '2024-02-08 15:22:18', 'rất ngon'),
(26, 1, 'PIZZA SEOUL BÒ XÀO BULGOGI VIỀN KHOAI LANG PHÔ MAI - SEOUL BEEF BULGOGI CHEESY SWEET POTATO CRUST', 'PIZZA+CUA+ST+(2).jpg', 199000, 20, '2024-02-08 15:23:01', 'rất ngon'),
(27, 3, ' Double Cheeseburger', 'burger_ga_pho_mai_so_t_bbq.jpg', 66000, 30, '2024-02-21 08:18:36', 'Double Cheeseburger'),
(28, 1, 'Pizza Phô Mai Cao Cấp/ Pizza Rau Củ) (Cỡ Vừa)', 'PIZZA+CUA+ST+(1).jpg', 249000, 10, '2024-02-22 03:10:08', 'Combo Chay - dành cho 3-4 người: 2 Pizza Chay (Pizza Phô Mai Cao Cấp/ Pizza Rau Củ) (Cỡ Vừa), 1 Chai Nước Ngọt 1.5L'),
(29, 1, 'pizza Phô Mai Cao Cấp', 'Pizza+Extra+Topping+(3).jpg', 250000, 20, '2024-03-20 08:42:12', 'ngon'),
(30, 1, 'PIZZA NEW YORK BÒ BEEFSTEAK', 'PC-MB1000X667px+super+topping@2x.png', 400000, 20, '2024-03-20 10:03:08', 'ngon'),
(31, 3, 'DOUBLE CHEESEBURGER', '2-mieng-bo-burger-b_-ph_-mai_1.jpg', 40000, 2, '2024-05-02 03:20:04', 'Burger 2 miếng bò nướng phô mai'),
(32, 3, 'WHOPPER BBQ BACON & CHEESE', '2-mieng-bo-burger-b_-ph_-mai_1.jpg', 50000, 33, '2024-05-02 03:38:33', 'Burger Whopper phô mai thịt xông khói ( cỡ lớn)'),
(33, 1, 'PIZZA 5 LOẠI THỊT THƯỢNG HẠNG - MEAT LOVERS', 'Meat-lover-Pizza-5-Loai-Thit-Thuong-Hang.jpg', 250000, 100, '2024-05-02 03:59:15', 'PIZZA 5 LOẠI THỊT THƯỢNG HẠNG - MEAT LOVERS');

-- --------------------------------------------------------

--
-- Table structure for table `product_categorys`
--

CREATE TABLE `product_categorys` (
  `id_product_categorys` int(11) NOT NULL,
  `categorys_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_categorys`
--

INSERT INTO `product_categorys` (`id_product_categorys`, `categorys_name`) VALUES
(1, 'Pizza'),
(2, 'Bún'),
(3, 'Hamburger'),
(4, 'bánh mì'),
(5, 'HotDog');

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `reviews_id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `review_date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`reviews_id`, `id_product`, `id_user`, `rating`, `comment`, `review_date`) VALUES
(1, 6, 11, 4, 'h', '2024-05-06'),
(2, 6, 11, 5, 'món này rất ngon', '2024-05-06'),
(3, 6, 11, 5, 'nó rất tệ', '2024-05-06'),
(4, 6, 11, 3, 'mnonadadadada', '2024-05-06'),
(5, 7, 11, 4, 'tuyệt vời', '2024-05-06'),
(6, 7, 11, 1, 'không ngon', '2024-05-06'),
(7, 6, 11, 1, 'không ngôn', '2024-05-06'),
(8, 7, 11, 4, 'rất tuyệt', '2024-05-06'),
(9, 7, 11, 3, 'cũng được', '2024-05-06'),
(10, 7, 11, 5, 'siêu tuyệt vời', '2024-05-06'),
(11, 8, 11, 4, 'bình thường', '2024-05-06'),
(12, 8, 11, 1, 'như >>', '2024-05-06'),
(13, 6, 11, 4, 'mmemem', '2024-05-07'),
(14, 10, 11, 4, 'cũng ngon đấy', '2024-05-07'),
(15, 10, 11, 5, 'rất tuyệt', '2024-05-07'),
(16, 10, 11, 2, 'bình thường', '2024-05-07'),
(17, 6, 11, 1, 'thật là tệ', '2024-05-07'),
(18, 8, 11, 5, 'rất tuyệt vời', '2024-05-07'),
(19, 8, 11, 4, 'ngon', '2024-05-07'),
(20, 8, 11, 3, 'cũng ngon', '2024-05-07'),
(21, 8, 11, 2, 'hủ', '2024-05-07'),
(22, 8, 11, 3, 'faefafa', '2024-05-07'),
(23, 8, 11, 4, 'dâdadadad', '2024-05-07'),
(24, 8, 11, 5, 'ãDdDdegdgrdg', '2024-05-07'),
(25, 8, 11, 3, 'ừeshrtfj5eg4ehz', '2024-05-07'),
(26, 16, 11, 2, 'fwefwesawefewf', '2024-05-07'),
(27, 16, 11, 4, 'grret4sdhdfjndfhdher', '2024-05-07'),
(28, 16, 11, 5, 'jyggdfcyfxfzhRbxd', '2024-05-07'),
(29, 16, 11, 5, 'ưerkfyrjhtzdbdzbgnjadgzbfSFGSRZthbz', '2024-05-07'),
(30, 16, 11, 1, 'regszvDRGSRzvDSF', '2024-05-07'),
(31, 16, 11, 1, 'éhyrurshbrgvreger', '2024-05-07'),
(32, 16, 11, 3, 'htxjstrjtrjsthabarbtjy', '2024-05-07'),
(33, 16, 11, 3, '32rdssvsdvswq', '2024-05-07');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `id_product_size` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

CREATE TABLE `size` (
  `id_size` int(11) NOT NULL,
  `name_size` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `size`
--

INSERT INTO `size` (`id_size`, `name_size`) VALUES
(5, 'M'),
(6, 'L'),
(8, 'XL'),
(9, 'X');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_web_ban_sua`
--

CREATE TABLE `user_web_ban_sua` (
  `id_user` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `user_address` varchar(100) NOT NULL,
  `phone` int(11) NOT NULL,
  `token` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `verify` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_web_ban_sua`
--

INSERT INTO `user_web_ban_sua` (`id_user`, `user_name`, `email_user`, `password`, `user_address`, `phone`, `token`, `date_added`, `verify`) VALUES
(1, 'name', 'nam@gmail.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', 'hanoi', 123456789, 828639, '2023-12-07 08:29:01', 0),
(4, 'quý', 'nqh41264@zslsz.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', 'hanoi', 123456789, 515629, '2023-12-07 10:10:46', 0),
(5, 'phong', 'csk01074@zslsz.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', '1111', 2147483647, 248650, '2023-12-11 02:17:19', 1),
(6, 'quý', 'hm@gmail.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', '1111111', 111111, 953486, '2023-12-13 06:54:00', 0),
(7, 'minh', 'gdc86822@zslsz.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', 'hanoi', 98561353, 307691, '2024-01-03 14:05:01', 0),
(8, 'minh', 'ihi52806@omeie.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', 'hanoi', 2147483647, 721784, '2024-01-04 08:59:27', 1),
(9, 'pham', 'pbp40089@zbock.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', 'hcm', 423356235, 861877, '2024-01-04 09:31:16', 1),
(10, 'a', 'oed11030@omeie.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', '1', 4231, 742546, '2024-02-21 04:14:47', 1),
(11, 'hung', 'toiuoc2001@gmail.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', 'hcm', 1223342, 736956, '2024-02-26 07:16:28', 1),
(13, 'quý', 'amk00549@zbock.com', 'ee79976c9380d5e337fc1c095ece8c8f22f91f306ceeb161fa51fecede2c4ba1', '35235', 45434, 532679, '2024-02-28 01:54:14', 1),
(14, 'fsgdfgd', 'sxu28892@ilebi.com', '3fe0aa7785f4f7d2a73371c99f925f41cfda333979014ffcfd8f60dee50730b8', '111111', 214325235, 893046, '2024-04-25 07:15:12', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id_order_detail`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id_order_item`),
  ADD KEY `id_order_detail` (`id_order_detail`);

--
-- Indexes for table `otp`
--
ALTER TABLE `otp`
  ADD PRIMARY KEY (`id_otp`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `id_product_categorys` (`id_product_categorys`);

--
-- Indexes for table `product_categorys`
--
ALTER TABLE `product_categorys`
  ADD PRIMARY KEY (`id_product_categorys`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`reviews_id`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id_product_size`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_size` (`id_size`);

--
-- Indexes for table `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`id_size`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `user_web_ban_sua`
--
ALTER TABLE `user_web_ban_sua`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id_order_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id_order_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `otp`
--
ALTER TABLE `otp`
  MODIFY `id_otp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `product_categorys`
--
ALTER TABLE `product_categorys`
  MODIFY `id_product_categorys` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `reviews_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id_product_size` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `size`
--
ALTER TABLE `size`
  MODIFY `id_size` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_web_ban_sua`
--
ALTER TABLE `user_web_ban_sua`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `id_user` FOREIGN KEY (`id_user`) REFERENCES `user_web_ban_sua` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `id_order_detail` FOREIGN KEY (`id_order_detail`) REFERENCES `order_detail` (`id_order_detail`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `id_product_categorys` FOREIGN KEY (`id_product_categorys`) REFERENCES `product_categorys` (`id_product_categorys`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_size`
--
ALTER TABLE `product_size`
  ADD CONSTRAINT `id_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_size` FOREIGN KEY (`id_size`) REFERENCES `size` (`id_size`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
