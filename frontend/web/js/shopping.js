// var searchInput = document.querySelector('.search-input');
// function searchFuntion() {
//     //var searchInput = document.querySelector('.search-input');
//     searchInput.style.display = 'block';
// }
// // document.onclick = function() {
// //     var searchInput = document.querySelector('.search-input');
// //     searchInput.style.display ='none'
// // }
// document.addEventListener('click', function(event) {
//     var isClickInside = searchInput.contains(event.target) || searchButton.contains(event.target);
//     if (!isClickInside) {
//         searchInput.style.display = 'none';
//     }
// });
document.addEventListener('DOMContentLoaded', function() {
    var searchInput = document.querySelector('.search-input');
    var searchButton = document.querySelector('.search-btn');
    searchButton.addEventListener('click', function() {
        searchInput.style.display = 'block';
        searchInput.focus();
    });
    document.addEventListener('click', function(e) {
        // console.log(e.target);
        var isClickInside = searchInput.contains(e.target) || searchButton.contains(e.target);
        // console.log(isClickInside);
        if (!isClickInside) {
            searchInput.style.display = 'none';
        }
    });
})
// viết sự kiên click vào menu trên header
var header = document.getElementById('header');
var mobileMenu = document.querySelector('.mobile-menu');
var currentHeader = header.clientHeight;
mobileMenu.onclick = function() {
    var isClose = header.clientHeight === currentHeader;
    if (isClose) {
        header.style.height = 'auto';
    } else {
        header.style.removeProperty('height');
    }
}
// ngăn chạn hành vi mặc định của thẻ a 
var categoriesElement = document.querySelector('li .categories');
categoriesElement.onclick = function(e) {
    let nav = document.getElementById('header');
    e.preventDefault();
    nav.style.overflow = 'initial';

}
// console.log(jsonData);
// 
//
$(document).ready(function() {
    $('.input-search').keyup(function() {
        let inputValue = $('.input-search').val();
        let searchResult = $('.search-result');
        $.post('/home/search', {
            'search': inputValue,
        })
        .done(function(data) {
            if(data) {
                $('.search-result').css({
                    'display': 'flex',
                    'flex-direction': 'column',
                });
                searchResult.html(data);
            } else {
                searchResult.css({
                    'display': 'none',
                });
            }
        })
    })
})
//
// add cart 
var btnAddCart = document.querySelectorAll('a.btn-add-cart');
for (let i = 0; i < btnAddCart.length; i++) {
    btnAddCart[i].onclick = function(e) {
        var href = btnAddCart[i].getAttribute('href');
        e.preventDefault();
        var addToCart = new XMLHttpRequest();
        addToCart.open('GET', href);
        addToCart.send();
        alert('Đã thêm vào giỏ hàng');
    }
}
// 
// tăng số lượng sản phẩm có trong gio hàng
var getQuantity = document.querySelectorAll('.so-luong');
var btnIncreaseCart = document.querySelectorAll('.increase-cart');
var btnDecreaseCart = document.querySelectorAll('.decrease-cart');
var productPrice = document.querySelectorAll('.gia-san-pham');
for(let i = 0; i < btnIncreaseCart.length; i++) {
    btnIncreaseCart[i].onclick = function(e) {
        //var href = btnIncreaseCart[i].getAttribute('href');
        e.preventDefault();
        let quantity= parseInt(getQuantity[i].value)+1;
        let urlIncreaseCart = btnIncreaseCart[i].search;
        let urlSearch = new URLSearchParams(urlIncreaseCart);
        let params = new FormData();
        params.append('id_product', urlSearch.get('id_product'));
        params.append('quantity', quantity);
        var increaseCart = new XMLHttpRequest();
        increaseCart.open('POST', 'home/update-cart');
        increaseCart.onload = function() {
            if (increaseCart.status >=200 && increaseCart.status < 300) {
                var response = JSON.parse(increaseCart.responseText);
                if (response) {
                    productPrice[i].innerHTML = response[urlSearch.get('id_product')]['product_price']*response[urlSearch.get('id_product')]['quantity'];
                    total();
                }
            }
        }
        increaseCart.send(params);
    }
}
for(let i = 0; i < btnDecreaseCart.length; i++) {
    btnDecreaseCart[i].onclick = function(e) {
        e.preventDefault();
        let quantity= parseInt(getQuantity[i].value)-1;
        let urlDecreaseCart = btnDecreaseCart[i].search;
        let urlSearch = new URLSearchParams(urlDecreaseCart);
        let params = new FormData();
        params.append('id_product', urlSearch.get('id_product'));
        params.append('quantity', quantity);
        var decreaseCart = new XMLHttpRequest();
        decreaseCart.open('POST', 'home/update-cart');
        decreaseCart.onload = function() {
            if (decreaseCart.status >=200 && decreaseCart.status < 300) {
                var response = JSON.parse(decreaseCart.responseText);
                if (response) {
                    productPrice[i].innerHTML = response[urlSearch.get('id_product')]['product_price']*response[urlSearch.get('id_product')]['quantity'];
                    total();
                }
            }
        }
        decreaseCart.send(params);
    }
}
// for(let i = 0; i < getQuantity.length; i++) {
//     getQuantity[i].onchange = function(e) {
//         // e.preventDefault();
//         let urlDecreaseCart = btnDecreaseCart[i].search;
//         let urlSearch = new URLSearchParams(urlDecreaseCart);
//         let params = new FormData();
//         params.append('id_product', urlSearch.get('id_product'));
//         params.append('quantity', getQuantity[i].value);
//         var quantity = new XMLHttpRequest();
//         quantity.open('POST', "home/update-cart");
//         // quantity.setRequestHeader('Content-Type', 'application/json');
//         // quantity.setRequestHeader("X-CSRF-Token", "<?= Yii::$app->request->csrfToken ?>");
//         quantity.send(params);
//     }
// }
for (let i = 0; i < getQuantity.length; i++) {
    getQuantity[i].onchange = function(e) {
        let urlDecreaseCart = btnDecreaseCart[i].search;
        let urlSearch = new URLSearchParams(urlDecreaseCart);
        let id_product = urlSearch.get('id_product');
        let quantity = getQuantity[i].value;

        // Tạo yêu cầu Ajax
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'home/update-cart', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        // Xử lý khi nhận được phản hồi từ server
        xhr.onload = function() {
            if (xhr.status >= 200 && xhr.status < 300) {
                // Phản hồi từ server
                var response = JSON.parse(xhr.responseText);
                //console.log(response[id_product]['product_price']);
                if (response) {
                    // Cập nhật giao diện người dùng với giá sản phẩm mới
                    productPrice[i].innerText = (response[id_product]['product_price']*response[id_product]['quantity']);
                    total();
                } else {
                    // Xử lý lỗi nếu cần
                }
            } else {
                // Xử lý lỗi nếu cần
            }
        };

        // Gửi yêu cầu đến server
        xhr.send('id_product=' + id_product + '&quantity=' + quantity);
    };
}
// 
// write js product review
$(document).ready(function() {
    let ratingData = 0;
    $('#add_review').click(function() {
        $("#review_modal").modal('show');
    });
    $(document).on('mouseenter', '.submit_star', function() {
        let rating = $(this).data('rating');
        resetColor();
        for (let index = 1; index <= rating; index++) {
            $("#submit_star_" + index).addClass('text-warning');
            ratingData =rating;
        }
    });
    // $(document).on('mouseleave', '.submit_star', function() {
    //     let rating = $(this).data('rating');
    //     for (let index = 1; index <= rating; index++) {
    //         $("#submit_star_" + index).removeClass('text-warning');
    //     }
    // });
    function resetColor() {
        for (let index = 1; index <= 5; index++) {
            $("#submit_star_" + index).addClass('star-light');
            $("#submit_star_" + index).removeClass('text-warning');
        }
    }
    $(document).on('click', '.submit_star', function() {
        ratingData = $(this).data('rating');
    });
    $("#save_review").click(function() {
        var userReview = $('#user_review').val();
        let url = window.location.href;
        let match = url.match(/\/(\d+)$/);
        let comments = $("#comments");
        let avergeRating = $("#average_rating");
        let totalReviews = $("#total_review");
        let totalFiveStar = $("#total_five_star_review");
        let totalFourStar = $("#total_four_star_review");
        let totalThreeStar = $("#total_three_star_review");
        let totalTwoStar = $("#total_two_star_review");
        let totalOneStar = $("#total_one_star_review");
        if (userReview == '') {
            alert('Vui lý nhập đánh giá');
            return false;
        } else {
            //console.log('trước khi thực hiện ajax')
            $.ajax({
                type: "POST",
                url: "/home/add-review",
                data: {rating_data: ratingData, user_review: userReview, id_product: parseInt(match[1])},
                success: function(data) {
                    //console.log("ajax add review");
                    $("#review_modal").modal('hide');
                    comments.html(data);
                    // 
                    // 
                    // 
                    $.ajax({
                        type: "POST",
                        url: "/home/update-rating",
                        data: {id_product: parseInt(match[1])},
                        success: function(data) {
                            // lấy json data;
                            //let obj = JSON.parse(data);
                            //console.log(data);
                            totalFiveStar.html(data['count_5']);
                            totalFourStar.html(data['count_4']);
                            totalThreeStar.html(data['count_3']);
                            totalTwoStar.html(data['count_2']);
                            totalOneStar.html(data['count_1']);
                            avergeRating.html(data['avergeRating']);
                            totalReviews.html(data['total']);
                        }
                    });
                }
                // 
                // 
            });
        }
    });
});

// 