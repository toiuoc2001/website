<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class AppAssetShop extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        // 'css/style.css',
        'css/style2.css'
    ];
    public $js = [
        'js/shopping.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap5\BootstrapAsset',
    ];
}
?>