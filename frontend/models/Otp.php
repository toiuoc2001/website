<?php
namespace frontend\models;

use yii\db\ActiveRecord;

class Otp extends ActiveRecord {
    public static function tableName() {
        return 'otp'; 
    }

    public function rules() {
        return [
            [['user_otp', 'otp_code'], 'required'],
        ];
    }

    public function attributeLabels() {
        return [
            'otp_user' => '',
            'otp_code' => '',
        ];
    }
}
?>