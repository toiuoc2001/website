<?php
namespace frontend\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class UserWeb extends ActiveRecord {

    public $password_b;
    public static function tableName() {
        return "user_web_ban_sua";
    }

    public function rules() {
        return [
            [['user_name', 'email_user', 'password', 'user_address', 'phone','password_b','token' ], 'required', 'message' => '{attribute} không được để trống'],
            [['user_name', 'email_user', 'password',], 'string'],
            ['email_user', 'email'],
            ['phone', 'integer', 'min' => 10],
        ];
    }

    public function attributeLabels() {
        return [
            'user_name' => '',
            'email_user' => '',
            'password' => '',
            'user_address' => '',
            'phone' => '',
            'password_b' =>''
        ];
    }

    
}
?>