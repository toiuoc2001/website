<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="sidebar">
    <a href="<?= Url::to(['home/filter-sort-pagination','filter'=>'pizza']) ?>" class="icon-pizza">
        <span class="material-symbols-outlined">
            local_pizza
        </span>
    </a>
    <a href="<?= Url::to(['home/filter-sort-pagination','filter'=>'hamburger']) ?>" class="icon-burger">
        <span class="material-symbols-outlined">
            lunch_dining
        </span>
    </a>
    <h1
    style="
    display: inline-block;
    line-height: 50px;
    float: right;
    font-size: 22px;
    background-color: #af2fd1;
    height: 50px;
    margin-top: 25px;
    border-radius: 10px;
    "
    >
        <a href="<?= Url::to(['home/show-all'])?>">XEM TẤT CẢ SẢN PHẨM</a>
    </h1>
</div>