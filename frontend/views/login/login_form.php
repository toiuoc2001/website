<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<section class="vh-100 gradient-custom" style ="margin-top: 46px;">
  <div class="container py-5 h-100">
    <div class="row justify-content-center align-items-center h-100">
      <div class="col-12 col-lg-9 col-xl-7">
        <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
          <div class="card-body p-4 p-md-5">
            <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Đăng nhập</h3>
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div style ="color: red">
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>
            <!-- <form>  --> <?php $form = ActiveForm::begin() ?>

              <div class="row">
                <div class="col-md-6 mb-4">

                  <div class="form-outline">
                    <!-- <input type="text" id="firstName" class="form-control form-control-lg" /> -->
                    <?= $form->field($model,'email_user')->input('text',['class' => "form-control form-control-lg", 'id' => 'email_user'])?>
                    <label class="form-label" for="firstName">email</label>
                  </div>

                </div>
              </div>

              <div class="row">
                <div class="col-md-6 mb-4 d-flex align-items-center">

                  <div class="form-outline datepicker w-100">
                    <!-- <input type="text" class="form-control form-control-lg" id="birthdayDate" /> -->
                    <?= $form->field($model, 'password')->input('password',['class' => "form-control form-control-lg", 'id' => 'password'])?>
                    <label for="birthdayDate" class="form-label">password</label>
                  </div>

                </div>
              </div>
              <div class="mt-4 pt-2">
                <input class="btn btn-primary btn-lg" type="submit" value="Login" />
                <div> 
                  <a href = "<?= Url::toRoute(['register']) ?>">Register ?</a>
                  <a href ="<?= Url::toRoute(['forgot-password']) ?>" style = "margin-left: 10px">Forgot password ? </a>
                </div>
              </div>
              <?php ActiveForm::end() ?>
            <!-- </form>  -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<style> 
.gradient-custom {
/* fallback for old browsers */
background: #f093fb;

/* Chrome 10-25, Safari 5.1-6 */
background: -webkit-linear-gradient(to bottom right, rgba(240, 147, 251, 1), rgba(245, 87, 108, 1));

/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
background: linear-gradient(to bottom right, rgba(240, 147, 251, 1), rgba(245, 87, 108, 1))
}

.card-registration .select-input.form-control[readonly]:not([disabled]) {
font-size: 1rem;
line-height: 2.15;
padding-left: .75em;
padding-right: .75em;
}
.card-registration .select-arrow {
top: 13px;
}
</style>
<script>
  window.onload = function() {
    let password = document.getElementById('password');
    password.value = '';
  }
</script>