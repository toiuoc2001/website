<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
 <div id="verify-otp">
        <?php 
        if (Yii::$app->session->hasFlash('success')) {
        echo '<div style ="color: green">' . Yii::$app->session->getFlash('success') . '</div>';
        } 
        ?>
            <h2> Vui lòng nhập mã OTP </h2>
            <p> Vui lòng nhập mã OTP đã được gửi đến email </p>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'style' => ['margin-top' => '20px'] ]]) ?>
            <?= $form ->field($model, 'otp_code')->input('text') ?>
            <button style ="width: 40%;margin-top: 20px;" type="submit" class="btn btn-primary" style = "width: 10%">Gửi</button>
            <?php ActiveForm::end() ?>
            <a href ="<?= Url::toRoute(['send-otp', 'id_user' => $_GET['id_user']]) ?>"> Gửi lại mã </a>
    </div>