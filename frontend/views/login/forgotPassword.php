<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id ="forgot-password">
        <div class="form-forgot-password">
            <h2>Quên mật khẩu</h2>
            <p>Vui lòng nhập email để lấy lại mật khẩu</p>
            <?php $form = ActiveForm::begin()  ?>
            <?= $form->field($model,'email_user')->input('text',['placeholder' => 'Vui lòng nhập email']) ?>
            <button style ="margin-top: 20px; width: 500px;" type="submit" class ="btn btn-success"> Gửi </button>
            <?php ActiveForm::end() ?>
        </div>
</div>