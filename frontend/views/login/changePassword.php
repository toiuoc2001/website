<?php
use yii\widgets\ActiveForm;
use Yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
    if (Yii::$app->session->hasFlash('error')) {
        echo "<div style = 'color: red'>" . Yii::$app->session->getFlash('error') . " </div>";
    }
    ?>
    <div id= "change-password">
            <h3>Thay đổi mật khẩu</h3>
            <p>Vui lòng nhập password mới</p>
            <?php $form = ActiveForm::begin() ?>
                <?= $form->field($model, 'password') -> input('password', ['style' => ['width' => '45%','display' => 'inline-block'], 'class' => 'password', 'value' => '']) ?>
                <div id = 'error' style = "margin: 20px; color: red"></div>
                <?= $form->field($model, 'password_b') -> input('password', ['style' => ['width' => '45%','display' => 'inline-block'], 'class' => 'password_b']) ?>
                <button type ='submit' class ='btn btn-primary' style = 'width: 10%; font-size:20px; margin-top: 20px' id = 'save-change-password' > Lưu </button>
            <?php ActiveForm::end() ?>
    </div>
</body>
</html>
<script>
    let password = document.querySelector('.password');
    let password_b = document.querySelector('.password_b');
    password_b.oninput = function () {
        if (password.value != password_b.value) {
            document.querySelector('#error').innerHTML = 'password không giống nhau';
        } else {
            document.querySelector('#error').innerHTML = '';
        }
    }
</script>
<style>
    #change-password {
        text-align: center;
        border: 1px solid;
        padding: 10px;
        border-radius: 10px;
    }
</style>