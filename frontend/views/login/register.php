<?php
use yii\helper\Html;
use yii\widgets\ActiveForm;
?>

<section class="vh-100 gradient-custom">
  <div class="container py-5 h-100">
    <div class="row justify-content-center align-items-center h-100">
      <div class="col-12 col-lg-9 col-xl-7">
        <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
          <div class="card-body p-4 p-md-5">
            <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Đăng ký</h3>
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div style ="color: red">
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>
            <!-- <form>  --> <?php $form = ActiveForm::begin() ?>

              <div class="row">
                <div class="col-md-6 mb-4">

                  <div class="form-outline">
                    <!-- <input type="text" id="firstName" class="form-control form-control-lg" /> -->
                    <?= $form->field($model,'email_user')->input('text',['class' => "form-control form-control-lg"])?>
                    <label class="form-label" for="firstName">email</label>
                  </div>

                </div>
                <div class="col-md-6 mb-4">

                  <div class="form-outline">
                    <!-- <input type="text" id="lastName" class="form-control form-control-lg" /> -->
                    <?= $form->field($model, 'user_name')->input('text',['class' => "form-control form-control-lg"])?>
                    <label class="form-label" for="lastName">Họ tên</label>
                  </div>

                </div>
              </div>

              <div class="row">
                <div class="col-md-6 mb-4">

                  <div class="form-outline">
                    <!-- <input type="text" id="firstName" class="form-control form-control-lg" /> -->
                    <?= $form->field($model,'password')->input('password',['class' => "form-control form-control-lg", 'maxLength' =>8, 'id' => 'password'])?>
                    <label class="form-label" for="firstName">password</label>
                  </div>

                </div>
                <div class="col-md-6 mb-4">
                <div style = "color: red" id ="show_error"> </div>
                  <div class="form-outline">
                    <!-- <input type="text" id="lastName" class="form-control form-control-lg" /> -->
                    <?= $form->field($model, 'password_b')->input('password',['class' => "form-control form-control-lg", 'maxLength' => 8,  'id' => 'password_b'])?>
                    <label class="form-label" for="lastName">nhập lại password</label>
                  </div>

                </div>
              </div>

              <div class="row">
                <div class="col-md-6 mb-4 pb-2">

                  <div class="form-outline">
                    <!-- <input type="email" id="emailAddress" class="form-control form-control-lg" /> -->
                    <?= $form->field($model,'user_address')->input('text',['class' => "form-control form-control-lg"])?>
                    <label class="form-label" for="emailAddress">Địa chỉ</label>
                  </div>

                </div>
                <div class="col-md-6 mb-4 pb-2">

                  <div class="form-outline">
                    <!-- <input type="tel" id="phoneNumber" class="form-control form-control-lg" /> -->
                    <?= $form->field($model,'phone')->input('text',['class' => "form-control form-control-lg"])?>
                    <label class="form-label" for="phoneNumber">Phone Number</label>
                  </div>

                </div>
              </div>
              <div class="mt-4 pt-2">
                <input class="btn btn-primary btn-lg" type="submit" value="Submit" id = "register"/>
              </div>
              <?php ActiveForm::end() ?>
            <!-- </form>  -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<style> 
.gradient-custom {
/* fallback for old browsers */
background: #f093fb;

/* Chrome 10-25, Safari 5.1-6 */
background: -webkit-linear-gradient(to bottom right, rgba(240, 147, 251, 1), rgba(245, 87, 108, 1));

/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
background: linear-gradient(to bottom right, rgba(240, 147, 251, 1), rgba(245, 87, 108, 1))
}

.card-registration .select-input.form-control[readonly]:not([disabled]) {
font-size: 1rem;
line-height: 2.15;
padding-left: .75em;
padding-right: .75em;
}
.card-registration .select-arrow {
top: 13px;
}
</style>