<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
$url1 = Url::to('@web/uploads/images/pizza-hub.jpg');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <!-- thông tin đơn hàng của người dùng nếu có -->
        <div class="title"><h2>Đơn đã đặt</h2></div>
        <?php if (isset($_SESSION['user_order']) ) { ?>
            <table style = "border: 1px solid">
                <tr style = " border: 1px solid">
                    <th style = "padding: 10px">Mã đơn</th>
                    <th style = "padding: 10px">Tên người đặt</th>
                    <th style = "padding: 10px">Số lượng</th>
                    <th style = "padding: 10px">Tổng tiền</th>
                </tr>
                <?php foreach ($userOrder as $v) { ?>
                <tr style = " border: 1px solid" >
                    <td style = "padding: 10px"><?php echo $v['id_order_detail'] ?></td>
                    <td style = "padding: 10px"><?php echo $v['user_name'] ?></td>
                    <td style = "padding: 10px"><?php echo $v['quantity'] ?></td>
                    <td style = "padding: 10px"><?php echo $v['total_price']  ?></td>
                </tr>
                <?php } ?>
            </table>
            <?php } else { ?>
                   <?php if (isset($_SESSION['id_user'])) {?>
                    <a> Hiện tại chưa có đơn hàng nào đã được đặt </a>
                   <?php } else {?>
                    <a> </a>
                    <?php } ?>
                <?php } ?>
        <!--  -->
        <div id="cart">
            <div class="title"><h2>Giỏ hàng</h2></div>
            <div class="products-cart">
            <?php $form = ActiveForm::begin(['action' => Url::toRoute(['home/oder']) , 'method' => 'post' ]) ?>
            <table class ="table">
                <tr class ="table-title">
                    <th>Tên sản phẩm</th>
                    <th>Hình ảnh</th>
                    <th>số lượng</th>
                    <th>giá sản phẩm</th>
                    <th colspan ="2"></th>
                </tr>
                <?php if (isset($_SESSION['item_cart'])) {?>
                <?php foreach($_SESSION['item_cart'] as $key => $value){ ?>
                <tr class = 'table-content'>
                    <td style = "border:none"><?php echo $value['product_name'] ?></td>
                    <td style = "border:none"> <img  src ='<?php echo('../images/'.$value['image']) ?>' style = "width:20%; height: auto"/></td>
                    <td style = "display: flex; border:none">
                        <div class = "btn-decrease-quantity" onclick ="decreaseValue(this)"><a class="decrease-cart" style ="text-decoration: none" href="<?= Url::to([ 'home/decrease-cart','id_product'=> $value['id_product'] ])?>">-</a></div>
                        <input id ='number'name ="quantity" class ="so-luong" type ='number'  value ='<?php echo $value['quantity'] ?>' min='1' max ='100' style ="text-align: center; font-size: 20px; border-radius: 10px;" >
                        <div class = "btn-increase-quantity" onclick = "increaseValue(this)"><a class="increase-cart" style ="text-decoration: none" href="<?= Url::to([ 'home/add-to-cart','id_product'=> $value['id_product'] ])?>">+</a></div>
                    </td>
                    <td style = "border:none" class = "gia-san-pham"><?php echo $value['product_price']*$value['quantity'] ?></td>
                    <td style = "border:none"><a href = "<?= Url::toRoute(['home/remove-cart', 'id_product' => $value['id_product']]) ?>" class ='btn btn-primary'  >xóa </a></td>
                </tr>
                <?php } ?>
                <tr>
                    <td>Tổng tiền: </td>
                    <td> </td>
                    <td> </td>
                    <td class ="total-quantity"></td>
                    <td class ="total">VND</td>
                </tr>
                <?php } ?>
            </table>
            <?php if (isset($_SESSION['item_cart']) && !empty( $_SESSION['item_cart'])) { ?>
                <?php if (isset($_SESSION['id_user']) ) {  ?>
                <button type="submit" class ='btn btn-primary' >Đặt mua</button>
                <?php } else { ?>
                <a href = "<?= Url::toRoute(['login/index']) ?>" class ='btn btn-primary' onclick= " alert('Vui lòng đăng nhập')">Đặt mua</a>
                <?php } ?>
            <?php } else { ?>
                <a href ="<?= Url::to(['home/index']) ?>" class ='btn btn-primary'> tiếp tục mua sắm</a>
            <?php }?>
            <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</body>
</html>
<script>
    var total = 0;
    var quantityTotal = 0
    var productPrice= document.querySelectorAll('.gia-san-pham');
    var productQuantity= document.querySelectorAll('.so-luong');
    var showTotal = document.querySelector('.total');
    var showTotalQuantity = document.querySelector('.total-quantity');
    for (let i = 0; i < productPrice.length; i++) {
        total += parseInt(productPrice[i].innerHTML);
    }
    for (let i = 0; i < productQuantity.length; i++) {
        quantityTotal += parseInt(productQuantity[i].value);
    }
    showTotal.innerHTML= total;
    showTotalQuantity.innerHTML= quantityTotal;
</script>
<script>
 function decreaseValue(element) {
    var input = element.nextElementSibling;
    var value = parseInt(input.value, 10);
    if (value > 1) {
      input.value = value - 1;
    }
  }

  function increaseValue(element) {
    var input = element.previousElementSibling;
    var value = parseInt(input.value, 10);
    input.value = value + 1;
  }
</script>
<script>
     function total () {
        var total = 0;
    var quantityTotal = 0
    var productPrice= document.querySelectorAll('.gia-san-pham');
    var productQuantity= document.querySelectorAll('.so-luong');
    var showTotal = document.querySelector('.total');
    var showTotalQuantity = document.querySelector('.total-quantity');
    for (let i = 0; i < productPrice.length; i++) {
        total += parseInt(productPrice[i].innerHTML);
    }
    for (let i = 0; i < productQuantity.length; i++) {
        quantityTotal += parseInt(productQuantity[i].value);
    }
    showTotal.innerHTML= total;
    showTotalQuantity.innerHTML= quantityTotal;
    }
</script>