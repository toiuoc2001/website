<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
// [ 'action' => Url::to([ 'home/add-to-cart','id_product'=> $_GET['id_product'] ]), 'options' => ['style' => 'display: flex'] ]
//var_dump($totalUserReview);
$total = 0;
foreach ($modelReviews as $value) {
    // var_dump ($value['rating']);
    $total = ($value['rating'] + $total);
}
if ($totalUserReview == 0) {
    $userRating = 0;
}else {
    $userRating = ($total / $totalUserReview);
}
$count_1 =0;
$count_2 = 0;
$count_3  = 0;
$count_4 =0;
$count_5 = 0;
foreach ($modelReviews as $value) {
    if ($value['rating'] == 1) {
        $count_1++;
    }
    if ($value['rating'] == 2) {
        $count_2++;
    }
    if ($value['rating'] == 3) {
        $count_3++;
    }
    if ($value['rating'] == 4) {
        $count_4++;
    }
    if ($value['rating'] == 5) {
        $count_5++;
    }
} 
// var_dump($total);
// var_dump($count_1, $count_2, $count_3, $count_4, $count_5);
?>
<?php $form = ActiveForm::begin([ 'action' => Url::to([ 'home/add-to-cart','id_product'=> $_GET['id_product'] ]), 'options' => ['style' => 'display: block'] ]); ?>
<div id="product-detail">
    <div class="product-info">
        <h1><?php echo $model->product_name; ?></h1>
        <i>
            <span class="material-symbols-outlined">
                grade
            </span>
            <span class="material-symbols-outlined">
                grade
            </span>
            <span class="material-symbols-outlined">
                grade
            </span>
            <span class="material-symbols-outlined">
                grade
            </span>
            <span class="material-symbols-outlined">
                grade
            </span>
            <p>4.5</p>
        </i>
        <p>TÌNH TRẠNG : <a style ="text-decoration: none;color: red; font-weight: 800;"> Còn Hàng</a> </p>
        <div>
            Số Lượng : 
            <input type="number" name="quantity" value="1" min = "1" max = "10">
        </div>
        <p style = "color: gray;"><?php echo number_format($model->product_price, 0, ',', '.'); ?> VND</p>
        <div style ="border-top: 0.6px solid; margin-top: 30px;">
            <div class ='service'>
                <a>phí dịch vụ</a>
                <p>0.0% Quán</p>
            </div>
            <p style ='display:inline-block ;font-size: 46px;font-weight: 100;'>|</p>
            <div class ='service'>
                <a>phí ship</a>
                <p>Food</p>
            </div>
        </div>
        <!-- button order -->
        <div class = "btn">
            <button class="order-btn">Đặt mua</button>
        </div>
        <!--  -->
    </div>
    <div class="product-image">
        <img src="<?php echo Url::to(['/images/'.$model->product_image], true) ?>" alt="">
    </div>
</div>
<?php ActiveForm::end();?>
<!-- <div class="product-comment">
    <h3>Đánh giá sản phẩm</h3>
    <div class="user-comment">
        <span class="material-symbols-outlined">
            person
        </span>
        <textarea name="" id="" cols="30" rows="2"></textarea>
        <a>Đăng bài viết</a>
    </div>
</div> -->
<div class="container">
    	<h1 class="mt-5 mb-5">Đánh giá người dùng</h1>
    	<div class="card">
    		<div class="card-header">đánh giá</div>
    		<div class="card-body">
    			<div class="row">
    				<div class="col-sm-4 text-center">
    					<h1 class="text-warning mt-4 mb-4">
    						<b><span id="average_rating"><?= round($userRating,1) ?></span> / 5</b>
    					</h1>
    					<div class="mb-3">
    						<i class="fas fa-star star-light mr-1 main_star"></i>
                            <i class="fas fa-star star-light mr-1 main_star"></i>
                            <i class="fas fa-star star-light mr-1 main_star"></i>
                            <i class="fas fa-star star-light mr-1 main_star"></i>
                            <i class="fas fa-star star-light mr-1 main_star"></i>
	    				</div>
    					<h3><span id="total_review"><?=$totalUserReview ?></span> Người đã đánh giá</h3>
    				</div>
    				<div class="col-sm-4">
    					<p>
                            <div class="progress-label-left"><b>5</b> <i class="fas fa-star text-warning"></i></div>

                            <div class="progress-label-right">(<span id="total_five_star_review"><?= $count_5 ?></span>)</div>
                            <div class="progress">
                                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="five_star_progress"></div>
                            </div>
                        </p>
    					<p>
                            <div class="progress-label-left"><b>4</b> <i class="fas fa-star text-warning"></i></div>
                            
                            <div class="progress-label-right">(<span id="total_four_star_review"><?= $count_4 ?></span>)</div>
                            <div class="progress">
                                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="four_star_progress"></div>
                            </div>               
                        </p>
    					<p>
                            <div class="progress-label-left"><b>3</b> <i class="fas fa-star text-warning"></i></div>
                            
                            <div class="progress-label-right">(<span id="total_three_star_review"><?= $count_3 ?></span>)</div>
                            <div class="progress">
                                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="three_star_progress"></div>
                            </div>               
                        </p>
    					<p>
                            <div class="progress-label-left"><b>2</b> <i class="fas fa-star text-warning"></i></div>
                            
                            <div class="progress-label-right">(<span id="total_two_star_review"><?= $count_2 ?></span>)</div>
                            <div class="progress">
                                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="two_star_progress"></div>
                            </div>               
                        </p>
    					<p>
                            <div class="progress-label-left"><b>1</b> <i class="fas fa-star text-warning"></i></div>
                            
                            <div class="progress-label-right">(<span id="total_one_star_review"><?= $count_1 ?></span>)</div>
                            <div class="progress">
                                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="one_star_progress"></div>
                            </div>               
                        </p>
    				</div>
    				<div class="col-sm-4 text-center">
    					<h3 class="mt-4 mb-3">Viết đánh giá</h3>
    					<?php if (isset($_SESSION['id_user'])) { ?>
                            <button type="button" name="add_review" id="add_review" class="btn btn-primary">Review</button>
                        <?php } else {?>
                            <a href="<?= Url::to(['login/index']) ?>" class="btn btn-primary">Đăng nhập để tạo bài viết</a>
                            <?php } ?>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="mt-5" id="review_content"></div>
    </div>

    <div class="modal fade" id="review_modal" tabindex="-1" aria-labelledby="review_modal_label" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<h5 class="modal-title" id="review_modal_label">Gửi đánh giá</h5>
	        	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      	</div>
	      	<div class="modal-body">
	      		<div class="text-center mt-2 mb-4">
	        		<i class="fas fa-star star-light submit_star mr-1" id="submit_star_1" data-rating="1"></i>
                    <i class="fas fa-star star-light submit_star mr-1" id="submit_star_2" data-rating="2"></i>
                    <i class="fas fa-star star-light submit_star mr-1" id="submit_star_3" data-rating="3"></i>
                    <i class="fas fa-star star-light submit_star mr-1" id="submit_star_4" data-rating="4"></i>
                    <i class="fas fa-star star-light submit_star mr-1" id="submit_star_5" data-rating="5"></i>
	        	</div>
	        	<div class="mb-3">
	        		<textarea maxlength="500" class="form-control" id="user_review" placeholder="Nhập đánh giá"></textarea>
	        	</div>
	        	<div class="text-center">
	        		<button type="button" class="btn btn-primary" id="save_review">Gửi</button>
	        	</div>
	      	</div>
    	</div>
  	</div>
</div>
<!-- comment user reviews -->
<div id="comments">
    <?php foreach($dataReviews as $data) { ?>
<div class="user-comments">
    <div class="comment-content">
        <div class="avatar-user">
            <img src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200" alt="">
        </div>
        <div class="comment-text">
            <p class="user-name"><?php echo $data['user_name']; ?> <span><?php echo $data['rating'] ?> &#9733;</span></p>
            <p class = "text"><?php echo $data['comment'] ?></p>
            <span><?php echo $data['review_date'] ?></span>
        </div>
    </div>
</div>
<?php } ?>
<!-- <button id = "read_more" data-model-target ="#model">Xem thêm bình luận</button> -->
</div>
<!--  -->