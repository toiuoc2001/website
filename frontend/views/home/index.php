<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
// $getUrl  =Yii::$app->request;
// $getDomain = $getUrl->getHostInfo();
// $url =Url::to(['home/index']);
//test url
// $replace = ['a','a','a','a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'd', 'e',
// 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o','o', 'o', 'o','o' ,'o' ,'o' ,'o' ,'o' ,'o' ,'o','o','o','o',
// 'u' , 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u','-'];
// $search = ['ã','à' , 'á' , 'ả' , 'ạ' ,'â', 'ẩ' , 'ấ','ẫ',
// 'ầ', 'ậ', 'ẵ','ă', 'ẳ', 'ằ' ,'ặ', 'ắ' , 'đ', 'ễ','ê', 'ể' ,'ế', 'ề' , 'ệ', 'ỉ', 'ì','ĩ',
// 'í', 'ị','õ', 'ò', 'ó', 'ọ', 'ỏ', 'ô', 'ỗ','ồ', 'ố', 'ộ', 'ổ', 'ỡ','ơ', 'ớ', 'ờ', 'ở', 'ù', 'ú','ủ', 'ụ',
// 'ư', 'ứ', 'ừ', 'ử', 'ự',' ', 
// ];
$replace = ['a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', '-',];
$search = ['ã', 'à', 'á', 'ả', 'ạ', 'â', 'ẩ', 'ấ', 'ẫ', 'ầ', 'ậ', 'ẵ', 'ă', 'ẳ', 'ằ', 'ặ', 'ắ', 'đ', 'ễ', 'ê', 'ể', 'ế', 'ề', 'ệ', 'ỉ', 'ì', 'ĩ', 'í', 'ị', 'õ', 'ò', 'ó', 'ọ', 'ỏ', 'ô', 'ỗ', 'ồ', 'ố', 'ộ', 'ổ', 'ỡ', 'ơ', 'ớ', 'ờ', 'ở', 'ù', 'ú', 'ủ', 'ụ', 'ư', 'ứ', 'ừ', 'ử', 'ự', ' ',];
?>
<!-- <div id="slider2"> -->
<?= $this->render('../shopping/slider.php'); ?>

</div>
<?= $this->render('../shopping/sidebar.php') ?>
<div>
<ul id="product-content-list">
    <?php foreach($model as $row) { 
        $str = str_replace($search, $replace, mb_strtolower($row['product_name']));
        ?>
    <li class="item">
        <div class="product-img">
            <a href="<?= Url::toRoute(['product-info', 'product_name' => $str, 'id_product' => $row['id_product'] ]) ?>" style ="text-decoration: none; color: black;">
                <img src="<?= Url::to(['/images/'.$row['product_image']], true) ?>" alt="">
            </a>
        </div>
        <div class="product-info">
            <h3 class="product-name">
                <a href ="<?= Url::toRoute(['product-info', 'product_name' => $str, 'id_product' => $row['id_product'] ]) ?>" style ="text-decoration: none; color: black;"><?php echo $row['product_name'] ?></a>
            </h3>
            <a class ="product-category">loại sản phẩm: 
                <a href ="<?= Url::to(['home/filter-sort-pagination', 'filter' => $row['categorys_name'] ]) ?>" style ="display: inline-block;text-decoration: none;"><?php echo $row['categorys_name'] ?></a>
            </a>
            <h3>
                <a class ="product-price"><?php echo number_format($row['product_price'], 0, ',', '.'); ?> VNĐ</a>
            </h3>
            <div>
                <i>Còn Hàng</i>
                <a href ="<?= Url::to(['home/add-to-cart', 'id_product' => $row['id_product']]) ?>" style = 'color: red' class ='btn-add-cart'>
                    <span style ="float:right;" class="material-symbols-outlined">
                    shopping_bag
                    </span>
                </a>
            </div>
        </div>
    </li>
    <?php }  ?>
</ul>
</div>
<?php $url = Url::to(['home/test']) ;
?>
<!-- <button onclick="functionTest()">test</button> -->