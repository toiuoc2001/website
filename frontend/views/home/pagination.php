<?php
use yii\helpers\Url;
$page = ceil($total/15);
 ?>
<!DOCTYPE html>
<html>
<head>
<style>
.pagination {
  display: inline-block;
}

.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid #ddd;
  margin: 0 4px;
}

.pagination a.active {
  background-color: #4CAF50;
  color: white;
  border: 1px solid #4CAF50;
}

.pagination a:hover:not(.active) {background-color: #ddd;}
</style>
</head>
<body>

<div class="pagination">
  <?php if (isset($_GET['page']) ) {
    if($_GET['page']<3) {
    ?>
    <?php } else { ?>
        <a href="<?=Url::to(['home/filter-sort-pagination', 'page' => $_GET['page']-1]) ?>">&laquo;</a>
        <?php }?>
    <?php }?>
    <a class="page" style ="background-color:red" href="<?= Url::to(['home/show-all']) ?>">1</a>
  <!--  -->
  <?php for($i=2;$i<=$page;$i++) { 
    if(isset($_GET['page'])) {
        if(isset($_GET['filter'])) {
            if(isset($_GET['sort'])) {
    ?>
    <!-- thuc hien page and sort with filter -->
    <?php if($i >= ($_GET['page']-2) && $i <= ($_GET['page']+2)) { ?>
        <a class="page" href="<?= Url::to(['home/filter-sort-pagination', 'filter' => $_GET['filter'], 'page' => $i, 'sort' => $_GET['sort']]) ?>"><?php echo $i ?></a>
        <?php } ?>
        <?php } else { ?>
            <!-- thuc hien page and filter -->
                <?php if($i >= ($_GET['page']-2) && $i <= ($_GET['page']+2)) { ?>
                <a class="page" href="<?= Url::to(['home/filter-sort-pagination', 'filter' => $_GET['filter'], 'page' => $i ]) ?>"><?php echo $i ?></a>
                <?php } ?>
            <?php }?>
        <!-- o day kiem tra dk co filter hay khong -->
        <?php } elseif (isset($_GET['sort'])) {?>
            <!-- o kiem tra dk co sort khong -->
                <?php if($i >= ($_GET['page']-2) && $i <= ($_GET['page']+2)) { ?>
                <a class="page" href="<?= Url::to(['home/filter-sort-pagination', 'page' => $i,'sort' => $_GET['sort'] ]) ?>"><?php echo $i ?></a>
                <?php } ?>
            <!-- thuc hien page and sort -->
            <?php } else { ?>
                <!-- thuc hien page -->
                    <?php if($i >= ($_GET['page']-2) && $i <= ($_GET['page']+2)) { ?>
                    <a class="page" href="<?= Url::to(['home/filter-sort-pagination', 'page' => $i, ]) ?>"><?php echo $i ?></a>
                    <?php } ?>
                <?php } ?>
        <!-- day trong dk kiem tra xem co $_GET[page] hay khong -->
    <?php } else {?>
    <!-- neu khong co page se thuc hien tao gia tri co page -->
    <?php if($i >= 2 && $i <= 6) { ?>
      <?php if(isset($_GET['filter'])) {?>
        <!-- kiểm tra co filter hay khong -->
        <?php if (isset($_GET['sort'])) {?>
            <!-- kiem tra co sort hay khong -->
            <a class="page" href="<?= Url::to(['home/filter-sort-pagination', 'filter' => $_GET['filter'], 'page' => $i, 'sort' => $_GET['sort']]) ?>"><?php echo $i ?></a>
            <?php } else {?>
                <!-- thuc hien page and filter -->
                <a class="page" href="<?= Url::to(['home/filter-sort-pagination', 'filter' => $_GET['filter'], 'page' => $i ]) ?>"><?php echo $i ?></a>
                <?php } ?>
        <?php } else { ?>
            <?php if (isset($_GET['sort'])) {?>
                <a class="page" href="<?= Url::to(['home/show-all', 'page' => $i ,'sort' => $_GET['sort']]) ?>"><?php echo $i ?></a>
                <?php } else {?>
                    <a class="page" href="<?= Url::to(['home/show-all', 'page' => $i ]) ?>"><?php echo $i ?></a>
                    <?php } ?>
            <?php } ?>
        <?php }?>
    <?php } ?>
  <!-- <a href="#"><?php echo $i ?></a> -->
  <?php }?>
  <?php if(isset($_GET['page'])) {
    if($_GET['page'] == $page or $_GET['page'] > $page) {
    ?>
    <?php } else {?>
        <!-- thuc hien tang so trang bang $_get[page]+1 -->
        <a href="<?=Url::to(['home/show-all', 'page' => $_GET['page']+1]) ?>">&raquo;</a>
        <?php } ?>
    <?php } else { ?>
        <!-- thuc hien tang so trong khi khon ton tai $get[page] -->
        <?php if(isset($_GET['filter'])) {?>
            <?php if (isset($_GET['sort'])) {?>
                <a href="<?=Url::to(['home/filter-sort-pagination', 'filter' => $_GET['filter'], 'page' => 2, 'sort' => $_GET['sort']]) ?>">&raquo;</a>
                <?php } else { ?>
                    <a href="<?=Url::to(['home/filter-sort-pagination', 'filter' => $_GET['filter'], 'page' => 2]) ?>">&raquo;</a>
                    <?php } ?>
            <?php }else { ?>
                <?php if (isset($_GET['sort'])) { ?>
                    <a href="<?=Url::to(['home/show-all', 'page' => 2, 'sort' => $_GET['sort']]) ?>">&raquo;</a>
                    <?php } else { ?>
                        <a href="<?=Url::to(['home/show-all', 'page' => 2]) ?>">&raquo;</a>
                        <?php } ?>
            <?php }?>
        <?php } ?>
</div>

</body>
</html>
 <script>
     var urlPara = new URLSearchParams(window.location.search);
     var paraValue = urlPara.get("page");
     var pageELement = document.querySelectorAll("a.page");
     if (paraValue) {
         for (let i = 0; i < pageELement.length; i++) {
             if (paraValue == parseInt(pageELement[i].textContent)) {
                 pageELement[i].style.backgroundColor = "red"; 
             } else {
                 pageELement[i].style.backgroundColor = "white";
             }
         }
     }
 </script>