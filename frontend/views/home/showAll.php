<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$page = ceil($total/15);
$replace = ['a','a','a','a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'd', 'e',
'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o','o', 'o', 'o','o' ,'o' ,'o' ,'o' ,'o' ,'o' ,'o','o','o','o',
'u' , 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u','-'];
$search = ['ã','à' , 'á' , 'ả' , 'ạ' ,'â', 'ẩ' , 'ấ','ẫ',
'ầ', 'ậ', 'ẵ','ă', 'ẳ', 'ằ' ,'ặ', 'ắ' , 'đ', 'ễ','ê', 'ể' ,'ế', 'ề' , 'ệ', 'ỉ', 'ì','ĩ',
'í', 'ị','õ', 'ò', 'ó', 'ọ', 'ỏ', 'ô', 'ỗ','ồ', 'ố', 'ộ', 'ổ', 'ỡ','ơ', 'ớ', 'ờ', 'ở', 'ù', 'ú','ủ', 'ụ',
'ư', 'ứ', 'ừ', 'ử', 'ự',' ', 
];
// foreach ($arrayTest as $t) {
//     //$text_1 = str_ireplace($search, $replace, strtolower($t));
//     //echo $text_1 . '<br>';
// }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="container" style ="    margin-top: 46px;">
        <div id="show-all">
            <div class="filter">
                <div class="filter-item">
                    <!-- tạo drop down menu-->
                    <a href ="<?= Url::toRoute(['home/show-all']) ?>" class = "high-light-link-1"  data-p="18">Tất cả</a>
                    <?php if(isset($_GET['sort'])) { ?>
                        <?php foreach ($categories as $c) { 
                            $categoriesName = str_ireplace($search, $replace, mb_strtolower($c['categorys_name'],"UTF-8"));
                            ?>
                        <a href ="<?= Url::toRoute(['home/filter-sort-pagination', 'filter' => "$categoriesName", 'sort' => "{$_GET['sort']}"]) ?>" class = "high-light-link-1"  data-value ="<?php echo  $categoriesName ?>" ><?php echo $c['categorys_name'] ?></a>
                        <?php } ?>
                    <?php }else {?>
                        <?php foreach ($categories as $c) { 
                            $categoriesName = str_ireplace($search, $replace, strtolower($c['categorys_name']));
                            ?>
                        <a href ="<?= Url::toRoute(['home/filter-sort-pagination', 'filter' => "$categoriesName"]) ?>" class = "high-light-link-1"  data-value ="<?php echo $categoriesName ?>"><?php echo $c['categorys_name'] ?></a>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class = "sort" style = "display:flex">
                    <?php if (isset($_GET['filter'])) { ?>
                    <a href="<?= Url::toRoute(['home/filter-sort-pagination','filter' =>"{$_GET['filter']}", 'sort' => 'sort-up']) ?>" data-value="sort-up" class ='sort-item' onclick = "highLightLink(event)"> giá tăng dần</a>
                    <a href="<?= Url::toRoute(['home/filter-sort-pagination','filter' =>"{$_GET['filter']}",'sort' => 'sort-down']) ?>" data-value="sort-down" class ='sort-item' onclick = "highLightLink(event)"> giá giảm dần</a>
                    <?php } else { 
                    ?>
                    <a href="<?= Url::toRoute(['home/show-all', 'sort' => 'sort-up']) ?>" data-value="sort-up" class ='sort-item' onclick = "highLightLink(event)"> giá tăng dần</a>
                    <a href="<?= Url::toRoute(['home/show-all','sort' => 'sort-down']) ?>" data-value="sort-down" class ='sort-item' onclick = "highLightLink(event)"> giá giảm dần</a>
                    <?php } ?>
                    <!-- <form action ="<?= Url::toRoute(['home/search']) ?>" method="post">
                        <div class ="search">
                            <input type = 'text' class ="search-input" name = 'search' id = 'search' placeholder = 'Tìm kiếm sản phẩm ...' oninput="searchNNN()">
                            <button type = 'submit' class ="search-btn">Tìm kiếm</button>
                        </div>
                    </form> -->
                </div>
            </div>
            <!-- Hiển thị tất cả các sản phẩm ở đây -->
        <ul class="product">
            <?php foreach ($model as $row) { 
                $str = str_replace($search, $replace, mb_strtolower($row['product_name']));
                ?>
            <li>
                <div class="product_item">
                    <div class ='product_top'>
                        <a href="<?= Url::toRoute(['product-info', 'product_name' => $str,'id_product' => $row['id_product']  ]) ?>" class="product_thumb">
                            <img src ='<?php echo Url::to(['/images/'.$row['product_image']], true) ?>'/>
                        </a>
                        <!-- <a href="<?= Url::toRoute(['home/add-to-cart', 'id_product' => $row['id_product'] ]) ?>" class="buy_now" >mua</a> -->
                        <!-- mua ngay --> 
                    </div>
                    <!-- thông tin sản phẩm -->
                    <div class="product_info">
                            <a href="" class="product_cart">loại sản phẩm: <?php echo($row['categorys_name']); ?></a>
                        <div style = 'display: flex'>
                            <a href="" class="product_name">tên sản phẩm :<?php echo($row['product_name']); ?></a>
                        <div  style ="width: 300px; height:200px; display: none"></div>
                        <!-- xem thêm -->
                            <a id="read_more" data-model-target ="#model"> Xem thêm</a> 
                            <div class ="show-text-read-more" id ="model">
                                <div class="show-header">
                                    <a class ="header-title"> <?php echo($row['categorys_name']); ?> </a>
                                    <button data-close-button class ="close-read-more">&times;</button>
                                </div>
                                <div class="show-body">
                                    tên sản phẩm :<?php echo($row['product_name']); ?>
                                </div>
                            </div>
                            <div id = "overlay"></div>
                            <!--  -->
                        </div>
                        <div class="product_price">giá sản phẩm:<a class ="price"> <?php echo($row['product_price']) ?></a></div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
 </div>
   <?php echo $this->render('pagination',['total'=> $total]); ?>
</div>
</body>
</html>
<script>
var currentURL = window.location.href;// lấy url trên của trang hiện tại
// Phân tích URL để lấy thông tin về tham số
var urlParams = new URLSearchParams(window.location.search);
// Lấy giá trị của tham số cụ thể
var parameterValue = urlParams.get('filter');
var getValue = document.querySelectorAll('a.high-light-link-1');
// In ra giá trị tham số
if (parameterValue) {
getValue.forEach(function (elementValue) {
     //console.log(elementValue.getAttribute("data-value"));
     elementValue.getAttribute("data-value");
     if(elementValue.getAttribute("data-value") === parameterValue) {
        //console.log(elementValue);
        elementValue.style.backgroundColor= "red";
     } else {
        elementValue.style.backgroundColor='#f4b342';
     }
});
} else {
    //console.log('khong co');
}
//
//tạo tính năng xem thêm;
var openReadMore = document.querySelectorAll('[data-model-target]');
var closeReadMore = document.querySelectorAll('[data-close-button]');
var overlay = document.getElementById('overlay');

openReadMore.forEach(function(element) {
    element.addEventListener('click', function() {
        var model = document.querySelector(element.dataset.modelTarget);
        openModel(model);
    })
});
closeReadMore.forEach(function(element) {
    element.addEventListener('click', function() {
        var model = element.closest('.show-text-read-more');
        closeModel(model);
    })
});
function openModel(model) {
    if (model == null) return;
    model.classList.add('active');
    overlay.classList.add('active');
}
function closeModel(model) {
    if (model == null) return;
    model.classList.remove('active');
    overlay.classList.remove('active');
};
// tim kiem

// searchInput.addEventListener('input', function() {
//     let value = searchInput.value.toLowerCase();
//     for (let i = 0; i < searchItem.length; i++) {
//         const itemText = searchItem[i].querySelector('a.product_name').textContent.toLowerCase();
//         if (itemText.includes(value)) {
//             searchItem[i].classList.remove('hidden');
//         } else {
//             searchItem[i].classList.add('hidden');
//         }
//     }
// });
function searchNNN() {
    const searchInput = document.getElementById('search');
    const searchItem = document.querySelectorAll('.product li');
    let value = searchInput.value.toLowerCase();
    for (let i = 0; i < searchItem.length; i++) {
        const itemText = searchItem[i].querySelector('a.product_name').textContent.toLowerCase();
        if (itemText.includes(value)) {
            console.log(searchItem[i]);
            searchItem[i].classList.remove('hidden5');
        } else {
            searchItem[i].classList.add('hidden5');
        }
    }
}
//hight light pagination trong trang
</script>
<!-- <script>
    var currentUrl = window.location.href; //lay url hien tai
    var urlPara = new URLSearchParams(window.location.search); //lay tham so trong url
    paraValue = urlPara.get("page");
    var pageELement = document.querySelectorAll("a.pagination");
    pageELement.forEach(function(element) {
        if (paraValue) {
            if (paraValue == parseInt(element.textContent)) {
                element.style.backgroundColor = "red";
            } else {
                element.style.backgroundColor = "white";
            }
        }
    });
</script> -->
<script>
    var urlPara = new URLSearchParams(window.location.search);
    var paraValue = urlPara.get("sort");
    var sortELement = document.querySelectorAll("a.sort-item");
    if (paraValue) {
        for (let i = 0; i < sortELement.length; i++) {
            if (paraValue === sortELement[i].getAttribute("data-value")) {
                sortELement[i].style.backgroundColor = "red";
            } else {
                sortELement[i].style.backgroundColor = "aqua";
            }
        }
    }
</script>
<style>
    .hidden5 {
    display: none;
}
</style>
