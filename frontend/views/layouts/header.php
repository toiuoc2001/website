<?php
use yii\helpers\Html;
use frontend\assets\AppAssetShop;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;
use yii\helpers\Url;
AppAssetShop::register($this);
$cssFile = Yii::$app->request->baseUrl.'/css/style.css';
$this->registerCssFile($cssFile);
/* @var $this yii\web\View */
/* @var $content string */
// if (isset($this->params['paras'])){
//     $this->registerJs(
//         "var jsonData = " . $this->params['paras'] . ";",
//         \yii\web\View::POS_HEAD // Chọn vị trí để nhúng mã JavaScript, ở đây là đầu trang
//     );
// }
?>
<?php $categories= Yii::$app->controller->getCategories();
$replace = ['a','a','a','a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'd', 'e',
'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o','o', 'o', 'o','o' ,'o' ,'o' ,'o' ,'o' ,'o' ,'o','o','o','o',
'u' , 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u','-'];
$search = ['ã','à' , 'á' , 'ả' , 'ạ' ,'â', 'ẩ' , 'ấ','ẫ',
'ầ', 'ậ', 'ẵ','ă', 'ẳ', 'ằ' ,'ặ', 'ắ' , 'đ', 'ễ','ê', 'ể' ,'ế', 'ề' , 'ệ', 'ỉ', 'ì','ĩ',
'í', 'ị','õ', 'ò', 'ó', 'ọ', 'ỏ', 'ô', 'ỗ','ồ', 'ố', 'ộ', 'ổ', 'ỡ','ơ', 'ớ', 'ờ', 'ở', 'ù', 'ú','ủ', 'ụ',
'ư', 'ứ', 'ừ', 'ử', 'ự',' ', 
];
 ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
    <link rel="icon" type="image/png" href="../assets/image/logo.png" style = "width: 500px; height: 500px;">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
</head>
<body>
<?php $this->beginBody() ?>
    <header>
        <div id = "header">
            <!-- bắt đầu navigation -->
            <ul id="nav">
                <li style ="display:inline-block"><a href="<?= Url::to(['home/index' ]) ?>">Home</a></li>
                <li><a href="<?= Url::to(['home/cart']) ?>">giỏ hàng</a></li>
                <li>
                    <a href="google.com" class ="categories">Danh mục
                        <span style ="position: absolute; top: 12px;}" class="material-symbols-outlined">
                            menu
                        </span>
                    </a>
                    <ul class="subnav">
                        <?php foreach ($categories as $c) { ?>
                        <li ><a href="<?= Url::to(['home/filter-sort-pagination','filter'=>str_ireplace($search, $replace, strtolower($c['categorys_name']))]) ?>"><?php echo $c['categorys_name'] ?></a></li>
                        <?php } ?>
                        <!-- <li ><a href="<?= Url::to(['home/filter-sort-pagination','filter'=>'buger']) ?>">burger</a></li> -->
                        <!-- <li ><a href="">spagheti</a></li> -->
                    </ul>
                </li>
            </ul>
            <!-- end navigation -->
            <!-- begin account -->
            <div class="account-btn">
                <?php if (isset($_SESSION['id_user'])) {?>
                    <a href="<?=Url::to(['login/logout']) ?>" style ="line-height: 46px; text-decoration: none; color: aqua;">Đăng xuất: <?php echo $_SESSION['name_user'] ?></a>
                <?php } else { ?>
                    <span style ="color: white; line-height: 46px; padding: 0 24px; cursor: pointer;" 
                    class="material-symbols-outlined">
                        account_circle
                    </span>
                    <ul class="account-nav">
                        <li><a href="<?= Url::to(['login/index'])?>">Đăng Nhập</a></li>
                        <li><a href="<?= Url::to(['login/register'])?>">Đăng ký</a></li>
                    </ul>
                    <?php }?>
            </div>
            <!-- end account -->
            <!-- begin search -->
            <div class="search-btn" >
                <span class="icon-search material-symbols-outlined">
                search
                </span>
                <div class="search-input">
                    <input class="input-search" type="text" placeholder="search">
                    <div class="search-result">
                        <a id = "url-search" href="" >
                            <img class ="image-result" src="https://www.apple.com/v/iphone/home/bt/images/overview/select/iphone_15_pro__bpnjhcrxofqu_xlarge.png" alt="HA">
                            <div class = "info-result">
                                <p class="name-result" >Điện Thoại Iphone 13</p>
                                <p class = "price-result">1000000 VNĐ</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="mobile-menu">
                <span class="icon-mobile-menu material-symbols-outlined">
                menu
                </span>
            </div>
            
            <!-- end search -->
        </div>
    </header>
    <main role="main" class="flex-shrink-0">
        <div>
            <?= $content ?>
        </div>
    </main>
    <!-- footer -->
    <!-- <footer>
        <p>Copyright &copy; 2022 - <span class="red">Food</span><span class="blue">Delivery</span></p>
    </footer> -->
    <!-- end footer -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<style>
    .red{
        color:red;
    }
    .blue{
        color:blue;
    }
    </style>
<!-- <script>
    var animationRpg=document.getElementById('home')
setInterval(function(){
    animationRpg.classList.toggle('red');
},900);
</script> -->