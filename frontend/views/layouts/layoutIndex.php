<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;
use yii\helpers\Url;
AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
</head>
<body>
<?php $this->beginBody() ?>
    <header>
        <div class = "div_header"> 
            <div class = "container" style = "display: flex;">
                <a class ="a" style = 'text-decoration: none;' href="<?= Url::toRoute(['home/index']) ?>" id = "home" > Home </a>
                <div class="dropdown">
                    <button class="dropbtn">Danh mục </button>
                    <div class="dropdown-content">
                        <a href = "">dddd</a>
                        <a href = "">b</a>
                        <a href = "">b</a>
                    </div>
                </div>
                <div>
                    <?php if (isset($_SESSION['id_user'])) { ?>
                    <a class ="dang_nhap a" href ="<?= Url::toRoute(['login/logout'])?>" style = "margin-left: 5px"> Đăng xuất: <?php echo $_SESSION['name_user'] ?> </a>
                        <?php } else {?>
                    <a class ="dang_nhap a" href ="<?= Url::toRoute(['login/index'])?>" style = "border-left: 1px solid; margin-left: 5px"> Đăng nhập </a>
                    <a href = "<?= Url::toRoute(['login/register']) ?>" class ="dang_nhap a"> Đăng ký </a>
                    <?php }?>
                </div>
                <a href ="<?= Url::toRoute(['home/cart']) ?>"><span class="material-symbols-outlined" style = "font-size: 30px">
                shopping_bag
                </span></a>
            </div>
        </div>
    </header>
    <main role="main" class="flex-shrink-0">
    <div>
        <?= $content ?>
    </div>
</main>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<style>
    .red{
        color:red;
    }
    .blue{
        color:blue;
    }
    </style>
<!-- <script>
    var animationRpg=document.getElementById('home')
setInterval(function(){
    animationRpg.classList.toggle('red');
},900);
</script> -->