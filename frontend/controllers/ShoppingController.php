<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;

class ShoppingController extends Controller
{
    public $layout = 'header';

    public function actionIndex() {
        return $this->render('showAllProducts');
    }
}
?>