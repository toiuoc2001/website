<?php
namespace frontend\controllers;

use yii\web\Controller;
use backend\models\Product;
use yii\db\Query;
use backend\models\ProductCategorys;
use backend\models\OrderDetail;
use backend\models\OrderItem;
use backend\models\ProductReviews;
use yii\helpers\Url;
use Yii;

class HomeController extends Controller {
    // public $layout = "layoutIndex";
    public $replace = ['a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', '-',];
    public $search = ['ã', 'à', 'á', 'ả', 'ạ', 'â', 'ẩ', 'ấ', 'ẫ', 'ầ', 'ậ', 'ẵ', 'ă', 'ẳ', 'ằ', 'ặ', 'ắ', 'đ', 'ễ', 'ê', 'ể', 'ế', 'ề', 'ệ', 'ỉ', 'ì', 'ĩ', 'í', 'ị', 'õ', 'ò', 'ó', 'ọ', 'ỏ', 'ô', 'ỗ', 'ồ', 'ố', 'ộ', 'ổ', 'ỡ', 'ơ', 'ớ', 'ờ', 'ở', 'ù', 'ú', 'ủ', 'ụ', 'ư', 'ứ', 'ừ', 'ử', 'ự', ' ',];
    public $layout = 'header';
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
            'captcha' => [
                'class' => \yii\captcha\CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        //$model = Product::find()->all();
        $query = new Query();
        $qr = new Query();
        $getDomain = Yii::$app->request->getHostInfo();
        $result = $query->select('*')
        ->from('product')
        ->innerJoin('product_categorys','product.id_product_categorys = product_categorys.id_product_categorys LIMIT 10')
        ->all();
        // $product = $qr->select('*')
        // ->from('product')
        // ->all();
        // foreach ($product as &$item) {
        //     $item['url'] = $getDomain.Url::to(['home/product-info', 'id_product' => $item['id_product']]);
        //     //var_dump($item);
        // }
        // $jsonProduct = json_encode($product);
        // $this->view->params['paras'] = $jsonProduct;
        return $this->render('index',['model' => $result]);
    }
    public function getCategories() {
        $query = new Query();
        $result = $query->select('*')
        ->from('product_categorys')
        ->all();
        return $result;
    }

    public function actionProductInfo($product_name,$id_product) {
        $model = Product::findOne($id_product);
        $qr = new Query();
        $qr->select("*")
        ->from("product_size")
        ->innerJoin('product','product.id_product = product_size.id_product')
        ->innerJoin('size','size.id_size = product_size.id_size')
        ->where("product.id_product = $id_product");
        $result = $qr->all();
        $query = new Query();
        $query->select('*')
        ->from("product_reviews")
        ->innerJoin('product','product.id_product = product_reviews.id_product')
        ->innerJoin('user_web_ban_sua','user_web_ban_sua.id_user = product_reviews.id_user')
        ->where(['product.id_product' => $id_product])
        ->limit(10);
        $dataReviews= $query->all();
        $getTotalUserReview = new Query();
        $getTotalUserReview->select('count(*)')
        ->from("product_reviews")
        ->innerJoin('product','product.id_product = product_reviews.id_product')
        ->innerJoin('user_web_ban_sua','user_web_ban_sua.id_user = product_reviews.id_user')
        ->where(['product.id_product' => $id_product]);
        $totalUserReview = $getTotalUserReview->all();
        // tạo thêm query load more comment 
        $modelReviews= ProductReviews::find()->where(['id_product' => $id_product])->all();
        $qrLoadMore = new Query();
        if (isset($_POST['page'])) {
            //  thưc hiện query load more comment
        }
        // 
        return $this->render('productInfo',['model' => $model,'result' => $result,'dataReviews' => $dataReviews, 'totalUserReview' => $totalUserReview[0]['count(*)'], 'modelReviews' => $modelReviews, ]);
    }

    // giỏ hàng
    public function actionAddToCart() {
        $query = new Query();
        if (Yii::$app->request->isPost) {
            if(!empty( $_POST['quantity'])) {
                $result = $query->select('*')
                ->from('product')
                ->where("id_product = $_GET[id_product]")
                ->all();
                $itemCart=[ $result[0]['id_product']=>[
                    'id_product' => $result[0]['id_product'],
                    'product_name' => $result[0]['product_name'],
                    'product_price' => $result[0]['product_price'],
                    'quantity' => (int)$_POST['quantity'],
                    'image' => $result[0]['product_image'],
                ], 
                ];
                if (!empty($_SESSION['item_cart'])) {
                    //foreach ($_SESSION['item_cart'] as $newArray) 
                    if(in_array($result[0]['id_product'],array_keys($_SESSION['item_cart'])) ) {
                        foreach($_SESSION['item_cart'] as $k => $v) {
                            if($k == $result[0]['id_product']) {
                                if(empty($_SESSION['item_cart'][$k]['quantity'])) {
                                    $_SESSION['item_cart'][$k]['quantity'] = (int)$_POST['quantity'];
                                }
                                $_SESSION['item_cart'][$k]['quantity'] += (int)$_POST['quantity'];
                            }
                        }
                    } else {
                        $_SESSION['item_cart'] = $_SESSION['item_cart']+ $itemCart;
                    }
                } else {
                    $_SESSION['item_cart'] =  $itemCart;
                    //var_dump($_SESSION['item_cart']);
                };
            }
        }else {
                $_POST['quantity'] = 1;
                $result = $query->select('*')
                ->from('product')
                ->where("id_product = $_GET[id_product]")
                ->all();
                $itemCart=[ $result[0]['id_product']=>[
                    'id_product' => $result[0]['id_product'],
                    'product_name' => $result[0]['product_name'],
                    'product_price' => $result[0]['product_price'],
                    'quantity' => (int)$_POST['quantity'],
                    'image' => $result[0]['product_image'],
                ], 
                ];
                if (!empty($_SESSION['item_cart'])) {
                    //foreach ($_SESSION['item_cart'] as $newArray) 
                    if(in_array($result[0]['id_product'],array_keys($_SESSION['item_cart'])) ) {
                        foreach($_SESSION['item_cart'] as $k => $v) {
                            if($k == $result[0]['id_product']) {
                                if(empty($_SESSION['item_cart'][$k]['quantity'])) {
                                    $_SESSION['item_cart'][$k]['quantity'] = (int)$_POST['quantity'];
                                }
                                $_SESSION['item_cart'][$k]['quantity'] += (int)$_POST['quantity'];
                            }
                        }
                    } else {
                        $_SESSION['item_cart'] = $_SESSION['item_cart']+ $itemCart;
                    }
                } else {
                    $_SESSION['item_cart'] =  $itemCart;
                };
        }
        return $this->redirect(['cart']);
    }
    public function actionDecreaseCart() {
        if(!empty($_SESSION['item_cart'])) {
            foreach($_SESSION['item_cart'] as $k => $v) {
                if((int)$_GET['id_product'] == $_SESSION['item_cart'][$k]['id_product'] ) {
                    if ($_SESSION['item_cart'][$k]['quantity'] == 1) {
                        unset($_SESSION['item_cart'][$k]);
                    } else {
                    $_SESSION['item_cart'][$k]['quantity'] -= 1;
                    }
                } 
            }
            return $this->redirect(['cart']); 
        }
    }
    public function actionUpdateCart() {
        header('Content-Type: application/json');
        yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
       if (!empty($_SESSION['item_cart'])) {
            foreach($_SESSION['item_cart'] as $k => $v) {
                if((int)$_POST['id_product'] == $_SESSION['item_cart'][$k]['id_product'] ) {
                    $_SESSION['item_cart'][$k]['quantity'] = (int)$_POST['quantity'];
                }
            }
            //var_dump($_SESSION['item_cart']);
            //return $this->redirect(['cart']);
            $dataJson = json_encode($_SESSION['item_cart']);
            echo $dataJson;
            die(); 
            return $this->render('cart');
        }
        // $quantity = Yii::$app->request->post('quantity');
        // var_dump($quantity);
        // echo"ko";
    }
    public function actionRemoveCart() {
        if(!empty($_SESSION['item_cart'])) {
            //unset($_SESSION['item_cart'][$_GET['id_product']]);
            foreach($_SESSION['item_cart'] as $k => $v) {
                if((int)$_GET['id_product'] == $_SESSION['item_cart'][$k]['id_product'] ) {
                    unset($_SESSION['item_cart'][$k]);
                    if(empty($_SESSION["cart_item"]))
				    unset($_SESSION["cart_item"]);
                } 
            }
            return $this->redirect(['cart']); 
        }
    }
    public function actionCart() {
        //kiểm tra người dùng có tòn tại đơn hàng đã đặt nào không
        if (isset($_SESSION['id_user'])) {
            $idUser = $_SESSION['id_user'];
            $query = new Query();
            $result = $query->select('*')
            ->from('order_detail')
            ->innerJoin('user_web_ban_sua', 'user_web_ban_sua.id_user = order_detail.id_user')
            ->where("user_web_ban_sua.id_user = $idUser and order_detail.status = 0")
            ->all();
            if (!empty($result)) {
                $_SESSION['user_order'] = $result;
                return $this->render('cart',['userOrder' => $result]);
            }
        }
        // truyền giá trị 
        return $this->render('cart');
    }

    public function actionOder() {
        if (isset($_SESSION['id_user']) && isset($_SESSION['verified']) == 1) {
            $quantityTotal = 0;
            $totalPrice = 0;
            $order_detail = new OrderDetail();
            //var_dump($_SESSION['item_cart']);
            foreach($_SESSION['item_cart'] as $a) {
                //lấy tổng số lượng sản phẩm và tiền 
                $quantityTotal = $quantityTotal+ $a['quantity'];
                $totalPrice += $a['product_price'] * $a['quantity'];
            }
            // echo "tổng số lượng sản phẩm :". $quantityTotal;
            // echo "tổng tiền". $totalPrice;
            if (Yii::$app->request->post()) {
                //echo "ddiek 1";
                $order_detail->quantity = $quantityTotal;
                $order_detail->total_price = $totalPrice;
                $order_detail->id_user = $_SESSION['id_user'];
                if ($order_detail->save()) {
                    //echo ("dk2");
                    foreach($_SESSION['item_cart'] as $value) {
                        $order_item = new OrderItem();
                        $order_item->quantity_order_item = $value['quantity'];
                        $order_item->price = $value['product_price'];
                        $order_item->id_order_detail = $order_detail->id_order_detail;
                        $order_item->id_product = $value['id_product'];
                        if($order_item->save()) {
                            $product = Product::findOne($value['id_product']);
                            $quantityUpdate = $product->quantity - (int)$value['quantity'];
                            $updateQuantityProduct = Yii::$app->db->createCommand()->update('product', ['quantity' =>$quantityUpdate], 'id_product = '.$value['id_product'])->execute();
                        } else {
                            echo "<script> alert('Đặt hàng thất bại') </script>";
                            return $this->redirect(['home/cart']);
                        }
                    }
                    return $this->render('orderSuccess');
                }
            }
        } else {
            // var_dump ($_SESSION['verified']);
            // echo "chưa được xác minh";
            return $this->redirect(['login/verify', 'id_user' => $_SESSION['id_user']]);
        }
    }
    // thực hiện hành động hiển thị tất cả sản phẩm
    /* 
    mô tả trang hiển thị dưới dang pagination trong trang tất cả sản phảm 
    đầu tiền kiểm tra giá trị $_get['page'] có trên url hay không 
    khởi tạo giá trị $page-1 = 0 và pageUp = 10 sau đó khi người dùng nhấn vào trang số n thì pageUp = *10
    */
    public function actionShowAll(){
        $categories = ProductCategorys::find()->all();
        $query = new Query();
        $qr= new Query();
        $total = $query->from("product")
        ->count("*");
        $result = $query->select('*')
        ->from('product')
        ->innerJoin('product_categorys','product.id_product_categorys = product_categorys.id_product_categorys LIMIT 15')
        ->all();
        //$a = json_encode($result);
        if (isset($_GET['sort'])) {
            if(isset($_GET['page'])) {
                $defaultPage = 15;//giá trị mặc định cho page lấy
                $page = 15 * ((int)$_GET['page'] -1);
                if ($_GET['sort'] == 'sort-up') {
                    $result = $qr->select('*')
                    ->from('product')
                    ->innerJoin('product_categorys',"product.id_product_categorys = product_categorys.id_product_categorys ")
                    ->orderBy("product_price ASC")
                    ->LIMIT( $defaultPage)
                    ->offSet($page)
                    ->all();
                    // $sql = $qr->createCommand()->getRawSql();
                    // // echo $sql;
                    // // echo $page;
                    // var_dump($result);
                    // die();
                } elseif ($_GET['sort'] == 'sort-down') {
                    $result = $qr->select('*')
                    ->from('product')
                    ->innerJoin('product_categorys',"product.id_product_categorys = product_categorys.id_product_categorys ")
                    ->orderBy("product_price DESC")
                    ->LIMIT($defaultPage)
                    ->offSet($page)
                    ->all();
                }
            } elseif($_GET['sort'] == 'sort-up') {
                $result = $qr->select('*')
                ->from('product')
                ->innerJoin('product_categorys',"product.id_product_categorys = product_categorys.id_product_categorys")
                ->orderBy("product.product_price ASC ")
                ->LIMIT(15)
                ->all();
            } elseif ($_GET['sort'] == 'sort-down') {
                $result = $qr->select('*')
                ->from('product')
                ->innerJoin('product_categorys',"product.id_product_categorys = product_categorys.id_product_categorys")
                ->orderBy("product_price DESC")
                ->LIMIT(15)
                ->all();
            }
        } else {
            if (isset($_GET['page'])) {
                $defaultPage = 15;//giá trị mặc định cho page lấy
                $page = 15 * ((int)$_GET['page'] -1);
                $total = $qr->from("product")
                ->count("*");
                $result = $qr->select('*')
                ->from('product')
                ->innerJoin('product_categorys',"product.id_product_categorys = product_categorys.id_product_categorys LIMIT $page, $defaultPage")
                ->all();
                return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
            }
        }
        return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
    }
    //
    // tạo chức năng tìm kiếm
    public function actionSearch() {
    //    header('content-type: application/json');
    //    Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        if (isset($_POST['search']) && $_POST['search'] != '') {
            if (Yii::$app->request->isPost) {
                $getDomain =  Yii::$app->request->getHostInfo();
                $query = new Query();
                //$qr = new Query();
                //$getDomain = Yii::$app->request->getHostInfo();
                $result = $query->select('*')
                ->from('product')
                ->innerJoin('product_categorys','product.id_product_categorys = product_categorys.id_product_categorys')
                ->where("product_name LIKE '%$_POST[search]%' ")
                ->limit(10)
                ->all();
                // tạo url cho từng sản phẩm
                if(!empty($result)) {
                    foreach ($result as &$item) {
                        if(isset($item['id_product'])){
                            $str = str_replace($this->search, $this->replace, mb_strtolower($item['product_name']));
                        $item['url'] = Url::to(['home/product-info','product_name'=>$str,'id_product' => $item['id_product']]);
                    }
                    }
                    // $jsonProduct = json_encode($result, JSON_UNESCAPED_UNICODE);
                    // echo $jsonProduct;
                    foreach($result as $value) {
                        echo "
                        <a id = 'url-search' href='$value[url]' >
                            <img class ='image-result' src='" . Url::to(['/images/' . $value['product_image']], true) . "' alt='img'>
                            <div class = 'info-result'>
                                <p class='name-result' >$value[product_name]</p>
                                <p class = 'price-result'>$value[product_price]</p>
                            </div>
                        </a>
                        ";
                    }
                    die();
                }
            }
        }else {
            // echo "";
            return null;
            die();
        }
    }
    //
    public function actionFilterSortPagination() {
        $categories = ProductCategorys::find()->all();
        $query = new Query();
        $qr = new Query();
        if(isset($_GET['filter'])) {
            if (isset($_GET['sort'])) {
               if ($_GET['sort'] == 'sort-up') {
                   if (isset($_GET['page'])) {
                    //thực hiện filter sort pagination ở dây
                    $defaultPage = 15;//giá trị mặc định cho page lấy
                    $page = 15 * ((int)$_GET['page'] -1);
                    $query->select('*')
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->where("product_categorys.id_product_categorys = '$_GET[filter]' ")
                    ->orderBy("product_price ASC")
                    ->limit( $defaultPage)
                    ->offSet($page);
                    $result = $query->all();
                    //lây tông số lượng sản phẩm
                    $total = $qr->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->where("product_categorys.id_product_categorys = '$_GET[filter]' ")
                    ->orderBy("product_price ASC")  
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                   } else {
                    //thực hiện filter cả sort ở đây
                    $defaultPage = 15;//giá trị mặc định cho page lấy
                    $result = $query->select('*')
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys ")
                    ->where("product_categorys.categorys_name = '$_GET[filter]' ")
                    ->orderBy("product_price ASC")
                    ->limit($defaultPage)
                    ->all();
                    $total = $qr
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->where("product_categorys.categorys_name = '$_GET[filter]' ")
                    ->orderBy("product_price ASC")
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                   }
               } elseif ($_GET['sort'] == 'sort-down') {
                   if (isset($_GET['page'])) {
                    $defaultPage = 15;//giá trị mặc định cho page lấy
                    $page = 15 * ((int)$_GET['page'] -1);
                    $query->select('*')
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys ")
                    ->where("product_categorys.categorys_name = '$_GET[filter]' ")
                    ->orderBy("product_price DESC")
                    ->limit( $defaultPage)
                    ->offSet($page);
                    $result = $query->all();
                    //lây tông số lượng sản phẩm
                    $total = $qr->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->where("product_categorys.categorys_name = '$_GET[filter]' ")
                    ->orderBy("product_price DESC")  
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                   } else {
                    $defaultPage = 15;
                    //thực hiện filter cả sort ở đây
                    $result = $query->select('*')
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->where("product_categorys.categorys_name = '$_GET[filter]' ")
                    ->orderBy("product_price DESC ")
                    ->limit($defaultPage)
                    ->all();
                    $total = $qr
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->where("product_categorys.categorys_name = '$_GET[filter]' ")
                    ->orderBy("product_price DESC")
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                   }
               }
            } else {
                if (isset($_GET['page'])) {
                    $defaultPage = 15;//giá trị mặc định cho page lấy
                    $page = 15 * ((int)$_GET['page'] -1);
                    // thực hiện filter và pagination
                    $defaultPage = 15;
                    $result = $query->select("*")
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys ")
                    ->where("product_categorys.categorys_name = '$_GET[filter]' LIMIT $page, $defaultPage")
                    ->all();
                    $total = $qr
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->where("product_categorys.categorys_name = '$_GET[filter]'")
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                } else {
                    $defaultPage = 15;
                    $result = $query->select("*")
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys ")
                    ->where("product_categorys.categorys_name = '$_GET[filter]' LIMIT $defaultPage")
                    ->all();
                    $total = $qr
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->where("product_categorys.categorys_name = '$_GET[filter]'")
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                }
            }
        } elseif(isset($_GET['sort'])) {
            if ($_GET['sort'] == 'sort-up') {
                if (isset($_GET['page'])) {
                    $defaultPage = 15;//giá trị mặc định cho page lấy
                    $page = 15 * ((int)$_GET['page'] -1);
                    $result= $query ->select('*')
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->orderBy("product_price ASC")
                    ->limit($defaultPage)
                    ->offSet($page)
                    ->all();
                    $total = $qr
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->orderBy("product_price ASC")
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                } else {
                    $defaultPage = 15;//giá trị mặc định cho page lấy
                    $result= $query ->select('*')
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->orderBy("product_price ASC")
                    ->limit($defaultPage)
                    ->all();
                    $total = $qr
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->orderBy("product_price ASC")
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                }
            } elseif ($_GET['sort'] == 'sort-down') {
                if (isset($_GET['page'])) {
                    $defaultPage = 15;//giá trị mặc định cho page lấy
                    $page = 15 * ((int)$_GET['page'] -1);
                    $result= $query ->select('*')
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys LIMIT $page, $defaultPage")
                    ->orderBy("product_price DESC")
                    ->all();
                    $total = $qr
                    ->from('product')
                    ->innerJoin('product_categorys','product.id_product_categorys = product_categorys.id_product_categorys')
                    ->orderBy("product_price DESC")
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                }else {
                    $defaultPage = 15;//giá trị mặc định cho page lấy
                    $result= $query ->select('*')
                    ->from('product')
                    ->innerJoin("product_categorys","product.id_product_categorys = product_categorys.id_product_categorys")
                    ->orderBy("product_price DESC")
                    ->limit($defaultPage)
                    ->all();
                    $total = $qr
                    ->from('product')
                    ->innerJoin('product_categorys','product.id_product_categorys = product_categorys.id_product_categorys')
                    ->orderBy("product_price DESC")
                    ->count("*");
                    return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
                }
            }
        } elseif (isset($_GET['page'])) {
            // thực hiện pagination
            $defaultPage = 15;//giá trị mặc định cho page lấy
            $page = 15 * ((int)$_GET['page'] -1);
            $total = $query->from("product")
            ->count("*");
            $result = $query->select('*')
            ->from('product')
            ->innerJoin('product_categorys',"product.id_product_categorys = product_categorys.id_product_categorys LIMIT $page, $defaultPage")
            ->all();
            return $this->render('showAll',['model' => $result, 'categories' => $categories, 'total' => $total]);
        } else {
            return $this->render(['notificationEndPage']);
        }
    }
    // tạo action review
    public function actionAddReview() {
        if (isset($_SESSION['id_user'])) {
            $model = new ProductReviews();
            if (Yii::$app->request->isPost) {
                if (Yii::$app->request->isAjax) {
                    if(isset($_POST['rating_data']) && isset($_POST['user_review']) && isset($_POST['id_product'])) {
                        $model->rating = $_POST['rating_data'];
                        $model->comment = $_POST['user_review'];
                        $model->id_product = $_POST['id_product'];
                        $model->id_user = $_SESSION['id_user'];
                        if ($model->save()) {
                            $qr = new Query();
                            $result = $qr->select('*')
                            ->from('product_reviews')
                            ->innerJoin('user_web_ban_sua','user_web_ban_sua.id_user = product_reviews.id_user')
                            ->where(['product_reviews.id_product' => $_POST['id_product']])
                            ->all();
                            if (!empty($result)) {
                                foreach ($result as $v) {
                                    echo 
                                    '
                                    <div class="user-comments">
                                        <div class="comment-content">
                                            <div class="avatar-user">
                                                <img src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200" alt="">
                                            </div>
                                            <div class="comment-text">
                                                <p class="user-name">'.$v['user_name'].'<span>'.$v['rating'].' &#9733;</span></p>
                                                <p class = "text">'.$v['comment'].'</p>
                                                <span>'.$v['review_date'].'/5</span>
                                            </div>
                                        </div>
                                    </div>
                                    ';
                        
                                }
                                die();
                            }
                        }
                    }
                }
            }
        } else {
            return NULL;
        }
    }
    public function actionUpdateRating() {
        header('Content-Type: application/json');
        yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $total = 0;
        $count_1 =0; $count_2 =0; $count_3 =0; $count_4 =0; $count_5 =0;
        //$_POST['id_product'] = 6;
        if (Yii::$app->request->isAjax) {
            if (isset($_POST['id_product'])) {
                $model = ProductReviews::find()->where(['id_product' => $_POST['id_product']])->all();
                foreach ($model as $value) { 
                    $total = $total + $value['rating'];
                    if ($value['rating'] == 1) {
                        $count_1++;
                    } elseif ($value['rating'] == 2) {
                        $count_2++;
                    } elseif ($value['rating'] == 3) {
                        $count_3++;
                    } elseif ($value['rating'] == 4) {
                        $count_4++;
                    } elseif ($value['rating'] == 5) {
                        $count_5++;
                    }
                }
                $array = [
                    'count_1' => $count_1,
                    'count_2' => $count_2,
                    'count_3' => $count_3,
                    'count_4' => $count_4,
                    'count_5' => $count_5,
                    'avergeRating' => round($total / count($model),1),
                    'total' => count($model),
                ];            
                //echo json_encode($array);
                //echo json_encode($array);
                return $array;
                //die();
            }
        }
    }
}

?>