<?php
namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\UserWeb;
use yii\db\Command;
use yii\db\Query;
use yii\helpers\Url;
use frontend\models\Otp;
use Yii;

class LoginController extends Controller {

    public $layout = "header";

    public function actionRegister() {
        $model = new UserWeb() ;
        if($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $token = mt_rand(000000,999999);
                $model->token = $token;
                $pass = hash('sha256', $model->password);
                $pass_b = hash('sha256', $model->password_b);
                $model->password = $pass;
                $model->password_b = $pass_b;
                $qr = new Query();
                $qr->select("*")
                ->from("user_web_ban_sua")
                ->where("email_user = '$model->email_user' ");
                $result = $qr->all();
                if (empty($result)) {
                   if ($model->password == $model->password_b) {
                        $model->save();
                        echo "<script> alert('Vui lòng xác minh tài khoản') </script>";
                        return $this->redirect(['verify', "id_user" => $model->id_user]);
                   } else {
                    Yii::$app->session->setFlash('error', 'mật khẩu không giống nhau');
                    return $this->redirect(['login/register']);
                   }
                } else {
                    //echo "<script> alert('tai khoản đã tồn tại') </script>";
                   Yii::$app->session->setFlash('error', 'tài khoản đã tồn tại');
                   return $this->redirect(['login/register']);
                }
            }
        } 
        return $this->render('register',['model' => $model]);
    }

    public function actionIndex() {
        $model = new UserWeb() ;
        if($model->load(Yii::$app->request->post()) ) {
            if (Yii::$app->request->isPost) {
                 $pass = hash('sha256', $model->password);
                 $model->password = $pass;
                 $qr = new Query() ;
                 $qr->select("*")
                 ->from("user_web_ban_sua")
                 ->where("email_user = '$model->email_user' AND password = '$model->password' ");
                 $result = $qr->all();
                 if (!empty($result)) {
                    if($result[0]['verify'] ==1) {
                        $_SESSION['id_user'] = $result[0]['id_user']; 
                        $_SESSION['name_user'] = $result[0]['user_name'];
                        $_SESSION['verified'] = $result[0]['verify'];
                        return $this->redirect(['home/index']);
                    } else {
                        echo "<script> alert('vui long xac minh tai khoan') </script>";
                        return $this->redirect(['verify', "id_user" => $result[0]['id_user']]);
                    }
                 } else {
                    //echo "<script> alert('tài khoản mật khẩu không chính xác') </script>";
                    Yii::$app->session->setFlash('error', 'sai email hoặc mật khẩu');
                    Yii::$app->session->setFlash('email', $model->email_user);
                    return $this->redirect(['login/index']);
                 }
            }
        }
        return $this->render('login_form', ['model'=>$model]);
    }

    public function actionLogout() {
        session_destroy();
        return $this-> redirect(['home/index']);
    }

    // queen mat khau
    public function actionForgotPassword() {
        $model = new UserWeb() ;
        if($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isPost) {
                // kiểm tra email có tồn tại trong csdl hay chưa.
                $query = new Query();
                $checkEmail = $query->select("*")
                ->from("user_web_ban_sua")
                ->where("email_user= '$model->email_user' ")->all();
                if(!empty($checkEmail)){
                    $OTP = mt_rand(000000,999999);
                    $send_mail = Yii::$app->mailer->compose()
                                ->setFrom("toiuoc12345@gmail.com")
                                ->setTo($model->email_user)
                                ->setSubject("ma xac nhan tai de lay lai password tai khoan")
                                ->setTextBody("ma xac nhan".$OTP);
                    if ($send_mail->send()) {
                        $qr = Yii::$app->db->createCommand()->insert('otp', ['user_otp' => $model->email_user, 'otp_code' => $OTP])->execute();
                        return $this->redirect(['verify-code', 'id_user' => $checkEmail[0]['id_user']]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'email khong ton tai');
                }
            }
        }
        return $this->render('forgotPassword',['model' => $model]);
    }

    public function actionVerifyCode() {
        $id_user = $_GET['id_user'];
        $model = new Otp();
        if ($model -> load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $qr = new Query() ;
                $result= $qr->select('*')
                        ->from('otp')
                        ->where("otp_code = '$model->otp_code' and time >= NOW()-INTERVAL 60 SECOND")
                        ->all();
                if(!empty($result)) {
                    return $this->redirect(['change-password','id_user' => $id_user ]);
                } else {
                    echo "<script> alert ('Mã OTP không đúng hoặc đã hết hạn ') </script>";
                }
            }
        }
        return $this->render('otpVerify',[ 'model' => $model ]);
    }

    //thực hiện thay đổi password
    public function actionChangePassword($id_user) {
        $model = UserWeb::findOne($id_user);
        if ($model->load(Yii::$app->request->post()) ) {
            if (Yii::$app->request->isPost) {
                if ($model->password == $model->password_b) {
                    $pass = hash('sha256', $model->password);
                    $model->password = $pass;
                    $model->save();
                    return $this->redirect(['login/index']);
                } else {
                    Yii::$app->session->setFlash('error', 'mật không giống nhau');
                    return $this->redirect(['change-password', 'id_user' => $id_user]);
                }
            }
        }
        return $this->render('changePassword',['model' => $model]);
    }

    public function actionVerify($id_user) {
        $getDomain = Yii::$app->request->getHostInfo();
        $model = UserWeb::findOne($id_user);
        $url = Url::toRoute(['login/verified', 'token' => "$model->token", 'id_user' => "$model->id_user"]);
        $send_mail = Yii::$app->mailer->compose()
                    ->setFrom("toiuoc12345@gmail.com")
                    ->setTo($model->email_user)
                    ->setSubject("email xác minh tài khoản")
                    ->setTextBody($getDomain.$url);
        //kiểm tra tài khoản đã được xác minh chưa nếu xác minh rồi thì chuyển hướng về login
        if ($model->verify == 1) {
            return $this->redirect(['login/index']);
        }else{
            $send_mail->send();
            echo "<script>alert('vui long check mail')</script>";
            return $this->render('verify');
        }
        // if ($send_mail->send()) {
        //     echo "<script>alert('vui long check mail')</script>";
        //     return $this->render('verify');
        // }
    }

    public function actionVerified() {
        //o day xac minh tai khoan nguoi dung thanh cong
        $token = $_GET['token'];
        $id_user = $_GET['id_user'];
        $qr = new Query() ;
        $result = $qr->select("*")
        ->from("user_web_ban_sua")
        ->where("token= $token and id_user = $id_user ")
        ->all();
        if (!empty($result)) {
            $update = Yii::$app->db->createCommand() -> update('user_web_ban_sua',['verify' => 1], "id_user = $id_user") -> execute(); 
            echo "<script>alert('xac minh thành công') </script>";
            return $this->redirect(['login/index']);
        } else { 
            Yii::$app->session->getFlash('error' , 'xác minh thất bại');
        }
        return $this->render('verified');
    }
    //
    public function actionSendOtp() {
        //o doan nay viet de gui lai ma OTP cho  nguoi dung
        Yii::$app->db->createCommand() -> delete('otp','time <= DATE_SUB(NOW(), INTERVAL 60 SECOND)')->execute();
        $id_user =$_GET['id_user'];
        $model = UserWeb::findOne($id_user);
        $OTP = mt_rand(100000,999999);
        $send_mail = Yii::$app->mailer->compose()
                    ->setFrom("toiuoc12345@gmail.com")
                    ->setTo($model->email_user)
                    ->setSubject('mã xác nhận:')
                    ->setTextBody($OTP);
        if($send_mail->send()) {
            $qr = Yii::$app->db->createCommand()->insert('otp', ['user_otp' => $model->email_user, 'otp_code' => $OTP])->execute();
            //echo "<script> alert('vui lòng kiểm tra email') </script>";
            Yii::$app->session->setFlash('success' , 'Vui lòng kiểm tra email');
            return $this->redirect(['login/verify-code', 'id_user' => $id_user]);
        }
    }
    public function getCategories() {
        $query = new Query();
        $result = $query->select('*')
        ->from('product_categorys')
        ->all();
        return $result;
    }
}
//m