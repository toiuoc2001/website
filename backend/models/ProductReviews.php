<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_reviews".
 *
 * @property int $reviews_id
 * @property int $id_product
 * @property int $id_user
 * @property float $rating
 * @property string $comment
 * @property string $review_date
 */
class ProductReviews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_product', 'id_user', 'rating', 'comment'], 'required'],
            [['id_product', 'id_user'], 'integer'],
            [['rating'], 'integer'],
            [['review_date'], 'safe'],
            [['comment'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reviews_id' => 'Reviews ID ',
            'id_product' => 'ID sản phẩm',
            'id_user' => 'Id User',
            'rating' => 'Rating',
            'comment' => 'Comment',
            'review_date' => 'Review Date',
        ];
    }
}
