<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class OrderDetail extends ActiveRecord
{
    public static function tableName()
    {
        return 'order_detail';
    }
    public function rules() {
        return [
            [[ 'total_price','id_user', 'quantity',], 'required'],
            [[ 'total_price', 'id_user', 'quantity', ], 'integer'],
        ];
    }
    public function attributeLabels() {
        return [
            'total_price' => '',
            'quantity' => '',
            'id_user' => '',
        ];
    }
}
?>