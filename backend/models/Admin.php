<?php
namespace backend\models;

use yii\db\ActiveRecord;

class Admin extends ActiveRecord {
    public $password_2 ;

    public static function tableName() {
        return "admin";
    }

    public function rules() {
        return [
            [['email', 'password', 'user_name', 'password_2', 'token'], 'required', 'message' => "chỗ này không được bỏ trống "],
            [['email', 'password', 'password_2', 'user_name'], 'string'],
            ['email', 'email'],
            [['password', 'password_2'],'string', 'min'=> 6 , 'tooShort' => "thấp nhất 6 ký tự"]
        ];
    }

    public function attributeLabels() {
        return [
            'email' => '',
            'password' => '',
            'user_name' => "",
            'password_2'=>"",
        ];
    }
}
?>