<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ProductReviews;

/**
 * ProductReviewsSearch represents the model behind the search form of `app\models\ProductReviews`.
 */
class ProductReviewsSearch extends ProductReviews
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reviews_id', 'id_product', 'id_user'], 'integer'],
            [['rating'], 'number'],
            [['comment', 'review_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductReviews::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'reviews_id' => $this->reviews_id,
            'id_product' => $this->id_product,
            'id_user' => $this->id_user,
            'rating' => $this->rating,
            'review_date' => $this->review_date,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
