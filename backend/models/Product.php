<?php
namespace backend\models;

use yii\db\ActiveRecord ;

class Product extends ActiveRecord {

    public static function tableName() {
        return 'product';
    }

    public function rules() {
        return [
            [['product_name', 'quantity', 'product_price', 'product_info','product_image'], 'required', 'message' => 'chỗ này không được bỏ trống'],
            [['product_name',], 'string'],
            [['quantity','id_product_categorys','product_price'], 'integer'],
            ['product_image', 'file', 'skipOnEmpty' => true, 'extensions' => "png, jpg, webp, avif" ]
        ];
    }

    public function attributeLabels() {
        return [
            'product_name' => '',
            'quantity' => '',
            'product_price' => '',
            'product_info' => '',
            'product_image' => '',
            'id_product_categorys' => ''
        ];
    }
}
?>