<?php
namespace backend\models;

use yii\db\ActiveRecord;

class ProductSize extends ActiveRecord {

    public static function tableName() {
        return 'product_size';
    }

    public function Rules() {
        return [
            [['id_product', 'id_size'], 'required'],
        ];
    }

    public function attributeLabels() {
        return [
            'id_product' => 'tên sản phẩm',
            'id_size' => 'kích thước sản phẩm',
        ];
    }
}
?>