<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class OrderItem extends ActiveRecord
{
    public static function tableName()
     { return 'order_item'; }

    public function rules() {
        return [
            [[ 'id_product','id_order_detail', 'quantity_order_item', 'price'], 'required'],
            [[ 'id_product', 'id_order_detail', 'quantity_order_item', 'price'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'id_product' => (''),
            'id_order_detail' => (''),
            'quantity' => (''),
            'price' => (''),
        ];
    }
}
?>