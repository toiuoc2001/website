<?php
namespace backend\models;

use yii\db\ActiveRecord;

class ProductCategorys extends ActiveRecord {
    public static function tableName() {
        return "product_categorys";
    }

    public function rules() {
        return [
            [['categorys_name'], 'required'],
            ['categorys_name', 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'categorys_name' => 'Tên danh mục sản phẩm',
        ];
    }
}
?>