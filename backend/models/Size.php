<?php
namespace backend\models;

use yii\db\ActiveRecord;

class Size extends ActiveRecord {
    public static function tableName() {
        return 'size';
    }

    public function Rules() {
        return [
            ['name_size','required'],
            ['name_size', 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'name_size' => 'kích thức sản phẩm',
        ];
    }
}
?>