<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\User;

class UserController extends Controller {
    public $layout = "webShopping";

    public function actionIndex() {
        if(isset($_SESSION['id_admin'])) {
        $model = User::find()->all();
        return $this->render('index',['model' => $model]);
        } else {
            return $this->redirect(['admin/login']);
        }
    }

    public function actionDelete($id_user) {
        if (isset($_SESSION['id_admin'])) {
        $model = User::findOne($id_user);
        $model->delete();
        return $this->redirect(['index']);
        } else {
            return $this->redirect(['admin/login']);
        }
    }
}
?>