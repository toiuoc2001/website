<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\Product;
use backend\models\Admin;
use backend\models\OrderDetail;
use backend\models\OrderItem;
use yii\db\Query;
use yii\helpers\Url;
use yii\db\Command;
use yii\filters\AccessControl;
use frontend\models\Otp;
use yii\filters\VerbFilter;

class AdminController extends Controller {

   public $layout = "webShopping";

    public function actionIndex() { 
        return $this->render('index');
    }

    public function actionLogin() {
        $model = new Admin() ;
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $pass = hash('sha256', $model->password);
                $model->password = $pass;
                $query = new Query();
                $result = $query->select("*")
                ->from("admin")
                ->where("email = '$model->email' AND password = '$model->password' ")
                ->all();
                if (!empty($result)) {
                    //kiểm tra tài khoản đã được xác minh hay chưa
                    if ($result[0]['verify'] == 1) {
                        $_SESSION['id_admin'] = $result[0]['id_admin'];
                        $_SESSION['name_admin'] = $result[0]['user_name'];
                        return $this->redirect(['index']);
                    } else {
                        echo "<script> alert('vui long xác minh tai khoan')</script>";
                        return $this->redirect(['admin/verify', 'id_admin' => $result[0]['id_admin']]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'sai email hoặc mật khẩu không đúng');
                }
            }
        }
        return $this->render('login', ['model' => $model]);
    }
    //đăng xuất 
    public function actionLogout() {
        session_destroy();
        return $this-> redirect(['admin/login']);
    }
    // đăng ký
    public function actionRegister() {
        $model = new Admin() ;
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $token = mt_rand(100000,999999);
                $model->token=$token;
                $pass=$model->password;
                $pass_2 = $model->password_2;
                $model->password  =hash('sha256', $pass);
                $model->password_2 = hash('sha256', $pass_2);
                $qr = new Query();
                $qr->select("*")
                ->from("admin")
                ->where("email = '$model->email' LIMIT 1 ");
                $result = $qr->all();
                if (empty($result)) {
                    if ($model->password == $model->password_2) {
                        $model->save();  
                        //echo "<script> alert('đăng ký thành công') </scritp>";
                        return $this->redirect(['verify','id_admin' => $model->id_admin]);
                    } else {
                        //echo "<script> alert('mật khẩu không khớp nhau') </scritp>";
                        Yii::$app->session->setFlash('error', 'mật khẩu không khớp nhau');
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'tài khoản đã tồn tại');
                }
            }
        } 
        return $this->render('register', ['model' => $model]);
    }

    public function actionVerify($id_admin) {
        $getDomain = Yii::$app->request->getHostInfo();
        $model = Admin::findOne($id_admin);
        $url = Url::toRoute(['admin/verified', 'id_admin' => "$id_admin" , 'token' => "$model->token"]);
        $sendMail = Yii::$app->mailer->compose()
                ->setFrom("toiuoc12345@gmail.com")
                ->setTo($model->email)
                ->setSubject("email xác minh tài khoản")
                ->setTextBody("ấn vào để xác minh tài khoản :".$getDomain.$url);
                // var_dump($sendMail->send()); die();
        if ($sendMail->send()) {
            echo "<script>alert('vui long check mail')</script>";
            return $this->render('verify');
        }
    } 

    public function actionVerified() {
         //o day xac minh tai khoan nguoi dung thanh cong
         $token = $_GET['token'];
         $idAdmin = $_GET['id_admin'];
         $qr = new Query() ;
         $result = $qr->select("*")
         ->from("admin")
         ->where("token= $token and id_admin = $idAdmin ")
         ->all();
         if (!empty($result)) {
             $update = Yii::$app->db->createCommand() -> update('admin',['verify' => 1], "id_admin = $idAdmin") -> execute(); 
             echo "<script>alert('xac minh thành công') </script>";
         } else { 
             Yii::$app->session->getFlash('error' , 'xác minh thất bại');
         }
         return $this->render('verified');
    }
    //tính năng quên mật khẩu
    public function actionForgotPassword() {
        $model = new Admin();
        $tokenUser = new Otp();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $qr = new  Query();
                $qr->select("*")
                ->from("admin")
                ->where("email = '$model->email' LIMIT 1 ");
                $data = $qr->one();
                if (!empty($data)) {
                    // var_dump($data['id_admin']);die;
                    $OTP = mt_rand(100000,999999);
                    $tokenUser->otp_code = $OTP;
                    $tokenUser->user_otp = $data['id_admin'];
                    if ($tokenUser->save()) {
                        $send_mail = Yii::$app->mailer->compose()
                        ->setFrom('toiuoc12345@gmail.com')
                        ->setTo($model->email)
                        ->setSubject('Mã xác minh')
                        ->setTextBody('Đây là mã xác minh: '.$OTP);
                       if ($send_mail->send()) {
                           echo "<script>alert('vui long check mail')</script>";
                           return $this->redirect(['verify-code', 'id_admin' => $tokenUser->user_otp]);
                       } 
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'tai khoan khong ton tai');
                }
            }
        }
        return $this->render('forgotPassword', ['model' => $model]);
    }
    // gửi lại otp cho người dùng
    public function actionSendOtp() {
        Yii::$app->db->createCommand() -> delete('otp','time <= DATE_SUB(NOW(), INTERVAL 60 SECOND)')->execute();
        $adminID = $_GET['id_admin'];
        $adminAccount = Admin::findOne($adminID);
        $OTP = mt_rand(100000,999999);
        $send_mail = Yii::$app->mailer->compose()
                ->setFrom('toiuoc12345@gmail.com')
                ->setTo($adminAccount->email)
                ->setSubject('Mã xác minh')
                ->setTextBody('Đây là má xác minh: '.$OTP);
                if ($send_mail->send()) {
                    $qr = Yii::$app->db->createCommand() ->insert('otp', ['user_otp' => $adminID, 'otp_code' => $OTP])->execute();
                    return $this->redirect(['verify-code', 'id_admin' => $adminID]);
                }
    }
    //xác minh otp 
    public function actionVerifyCode() {
        $model = new Otp();
        $adminID = $_GET['id_admin'];
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $qr = new Query();
                $checkToken = $qr->select("*")
                ->from("otp")
                ->where("otp_code = $model->otp_code and time >= NOW()-INTERVAL 60 SECOND")
                ->andWhere("user_otp = $adminID ")
                ->all();
                if (!empty($checkToken)) {
                    return $this->redirect(['change-password', 'id_admin' => $adminID]);
                } else {
                    echo "<script>alert('Mã OTP không đúng hoặc không hết hạn')</script>";
                }
            }
        }
        return $this->render('verifyOtp', ['model' => $model]);
    }
    //thay đổi mật khẩu 
    public function actionChangePassword() {
        $model = Admin::findOne($_GET['id_admin']);
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $model->password = hash('sha256', $model->password);
                $model->password_2 = hash('sha256', $model->password_2); 
                if ( $model->password == $model->password_2) {
                    $model->save();
                    echo "<script>alert('mật khẩu đã được thay đổi')</script>";
                    return $this->redirect(['login']);
                } else {
                    echo "<script>alert('mật không khop nhau')</script>";
                }
            }
        }
        return $this->render('changePassword', ['model' => $model]);
    }
    // kết thúc tính năng quên mật khẩu
    //đơn hàng
    public function actionOrder() {
        if (isset($_SESSION['id_admin'])) { 
            $query = new Query();
            $result = $query->select("*")
                ->from("order_detail")
                ->innerJoin('user_web_ban_sua','user_web_ban_sua.id_user = order_detail.id_user')
                ->all();
            return $this->render('orderDetail',['data'=>$result]);
        } else {
            return $this->redirect(['admin/login']);
        }
    }
    // chi tiet san pham trong don hang
    public function actionOrderDetail() {
        if (isset($_SESSION['id_admin'])) {
            if (isset ($_GET['id_order_detail'])) {
            $id_order_detail = $_GET['id_order_detail'];
            $query = new Query();
            $result = $query->select("*")
                    ->from("order_item")
                    ->innerJoin('product',' product.id_product = order_item.id_product')
                    ->where("order_item.id_order_detail = $id_order_detail")
                    ->all();
            return $this->render('orderItemDetail',['model'=>$result]);
            } else {
                return $this->redirect(['admin/order']);
            }   
        } else {
            return $this->redirect(['admin/login']);
        }
    }
    // xoa san pham trong don hang
    public function actionDeleteOrderItem() {
        if (isset($_SESSION['id_admin'])) {
            $id_order_item = $_GET['id_order_item'];
            $model = OrderItem::findOne($id_order_item);
            if($model->delete()) { 
                $orderDetail = OrderDetail::findOne($model->id_order_detail);
                if ( $orderDetail->quantity ==0 ) {
                    $orderDetail->delete();
                } else {
                    $quantityUpdate = $orderDetail->quantity - $model->quantity_order_item;
                    $update = Yii::$app->db->createCommand()->update('order_detail', ['quantity' => $quantityUpdate], 'id_order_detail = '.$model->id_order_detail)->execute();
                }
            }
        } else {
            return $this->redirect(['admin/login']);
        }
        //cap nhat so luong trong don hang
        
        return $this->redirect(['admin/order']);
    }
    // xác nhận đơn hàng
    public function actionOrderConfirmation() {
        if (isset($_SESSION['id_admin'])) {
            $id_order_detail = $_GET['id_order_detail'];
            $status=1;//trang thai da duyet
            $orderDetail = OrderDetail::findOne($id_order_detail);
            $orderDetail->status=$status;
            if($orderDetail->save()) {
                echo "<script>atlert('đơn hàng đã được xác nhận')</script>";
                return $this->redirect(['admin/order']);
            } else {
                echo "<script>atlert('đơn hàng chưa được xác nhận')</script>";
                return $this->redirect(['admin/order']);
            }
        } else {
            return $this->redirect(['admin/login']);
        }
    }
    //kết thúc
    // xóa đơn hàng
    public function actionDeleteOrder() {
        if (isset($_SESSION['id_admin'])) {
            $id_order_detail = $_GET['id_order_detail'];
            $model = OrderDetail::findOne($id_order_detail)->delete();
            return $this->redirect(['admin/order']);
        } else {
            return $this->redirect(['admin/login']);
        }
    }
    //
}
?>