<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\Product;
use yii\web\UploadedFile;
use backend\models\Size;
use backend\models\ProductSize;
use yii\db\Query;
use backend\models\ProductCategorys;
use yii\filters\AccessControl;

class ProductController extends Controller {

    public $layout = "webShopping";

    public function actionIndex() {
        //$model = Product::find()->all();
        if (isset($_SESSION['id_admin'])) {
            $query = new Query();
            $result = $query->select("*")
            ->from("product")
            ->innerJoin('product_categorys','product.id_product_categorys = product_categorys.id_product_categorys')
            ->all();
            return $this->render('index',['model' => $result]);
        } else {
            return $this->redirect(['admin/login']);
        }
    }

    public function actionCreate() {
        if (isset($_SESSION['id_admin'])) {
        $model = new Product() ;
        $categorys = ProductCategorys::find()->all();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $model->product_image = UploadedFile::getInstance($model, 'product_image');
                $model->product_image->saveAs('../web/imgs/'.$model->product_image->basename.'.'.$model->product_image->extension,false);
                if ($model->save()) {
                    if (file_exists('../web/imgs/'.$model->product_image->basename.'.'.$model->product_image->extension)) {
                        $sourcePath = '../web/imgs/'.$model->product_image->basename.'.'.$model->product_image->extension;
                        $destinationPath = Yii::getAlias('@frontend/web/images/').$model->product_image->basename.'.'.$model->product_image->extension;
                        copy($sourcePath, $destinationPath);
                    } else {
                        Yii::$app->session->getFlash('error', 'Hình ảnh chưa được lưu');
                    }
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('form',  ['model' => $model, 'categorys' => $categorys]);
    }else {
        return $this->redirect(['admin/login']);
    }
    }

    public function actionUpdate($id_product) {
        if (isset($_SESSION['id_admin'])) {
        $model = Product::findOne($id_product) ;
        $categorys = ProductCategorys::find()->all();
        // $old_image = $model -> product_image ;
        if ($model->load(Yii::$app->request->post()) ) {
            if (Yii::$app->request->isPost) { 
                $model->product_image = UploadedFile::getInstance($model, 'product_image');
                if(!empty($model->product_image)) { 
                    $model->product_image->saveAs('../web/imgs/'.$model->product_image->basename.'.'.$model->product_image->extension,false);
                } else {
                    $model->product_image = $old_image; 
                }
                if ($model->save()) {
                    if (file_exists('../web/imgs/'.$model->product_image->basename.'.'.$model->product_image->extension)) {
                        $sourcePath = '../web/imgs/'.$model->product_image->basename.'.'.$model->product_image->extension;
                        $destinationPath = Yii::getAlias('@frontend/web/images/').$model->product_image->basename.'.'.$model->product_image->extension;
                        copy($sourcePath, $destinationPath);
                    } else {
                        Yii::$app->session->getFlash('error', 'Hình ảnh chưa được lưu');
                    }
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('form' , ['model' => $model,'categorys' => $categorys]);
    }else {
        return $this->redirect(['admin/login']);
    }
    }

    public function actionDelete($id_product) {
        if(isset($_SESSION['id_admin'])) {
        $model = Product::findOne($id_product)->delete();
        return $this->redirect(['index']);
        }else {
            return $this->redirect(['admin/login']);
        }
    }

    public function actionSize() {
        if (isset($_SESSION['id_admin'])) {
        $model = Size::find()->all();
        $qr = new Query();
        $qr->select("*")
        ->from("product_size")
        ->innerJoin('product','product.id_product = product_size.id_product')
        ->innerJoin('size','size.id_size = product_size.id_size');
        $result = $qr->all();
        return $this->render('size',['model' => $model, 'result' => $result]);
        } else {
            return $this->redirect(['admin/login']);
        }
    }
    public function actionCreateSize() {
        if (isset($_SESSION['id_admin'])) {
        $model = new Size();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
            echo "<script> alert('thêm thành công') </script>";
            return $this->redirect(['size']);
        }}  
        return $this->render('formCreateSize',['model' => $model]); 
    }else {
        return $this->redirect(['admin/login']);
    }
    }

    public function actionUpdateSize($id_size) {
        if (isset($_SESSION['id_admin'])) {
        $model = Size::findOne($id_size);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
            echo "<script> alert('sửa thành công') </script>";
            return $this->redirect(['size']);
        }}  
        return $this->render('formCreateSize',['model' => $model]); 
    }else {
        
    }
    }
    public function actionDeleteSize($id_size) {
        if(isset($_SESSION['id_admin'])) {
        $model = Size::findOne($id_size)->delete();
        return $this->redirect(['size']);
        }else {
            return $this->redirect(['admin/login']);
        }
    }
    public function actionAddSize() {
        if (isset($_SESSION['id_admin'])) {
        $size = Size::find()->all();
        $product = Product::find()->all();
        $model = new ProductSize();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
                return $this->redirect(['size']);
            }
        }
        return $this->render('formAddSize',['model' => $model, 'size' => $size, 'product' => $product]);
    }else {
        return $this->redirect(['admin/login']);
    }
    }

    public function actionUpdateProductSize($id_product_size) {
        if (isset($_SESSION['id_admin'])) {
        $size = Size::find()->all();
        $product = Product::find()->all();
        $model = ProductSize::findOne($id_product_size);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
                return $this->redirect(['size']);
            }
        }
        return $this->render('formAddSize',['model' => $model, 'size' => $size, 'product' => $product]);
    }else {
        return $this->redirect(['admin/login']);
    }
    }
    
    public function actionDeleteProductSize($id_product_size) {
        if (isset($_SESSION['id_admin'])) {
        $model = ProductSize::findOne($id_product_size)->delete();
        return $this->redirect(['size']);
        }else {
            return $this->redirect(['admin/login']);
        }
    }
    //danh mục sản phẩm
    public function actionCategorys() {
        if (isset($_SESSION['id_admin'])) {
        $model = ProductCategorys::find()->all();
        return $this->render('productCategorys',['model' => $model]);
        } else {
            return $this->redirect(['admin/login']);
        }
    }
    //thêm danh mục sản phẩm
    public function actionCreateCategorys() {
        if (isset($_SESSION['id_admin'])) {
        $model = new ProductCategorys();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
            echo "<script> alert('thêm ReferentialAction') </script>";
            return $this->redirect(['categorys']);
        }}  
        return $this->render('formCreateCategorys',['model' => $model]); 
    }else {
        return $this->redirect(['admin/login']);
    }
    }
    //update danh mục sản phẩm
    public function actionUpdateCategorys($id_product_categorys) {
        if (isset($_SESSION['id_admin'])) {
        $model = ProductCategorys::findOne($id_product_categorys);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
            return $this->redirect(['categorys']);
        }}  
        return $this->render('formCreateCategorys',['model' => $model]); 
    }else {
        return $this->redirect(['admin/login']);
    }
    }
    //xóa danh mục.sulieu
    public function actionDeleteCategorys($id_product_categorys) {
        if(isset($_SESSION['id_admin'])) {
        $model = ProductCategorys::findOne($id_product_categorys)->delete();
        return $this->redirect(['categorys']);
        }else {
            return $this->redirect(['admin/login']);
        }
    }
}
?>