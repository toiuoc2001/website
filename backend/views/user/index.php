<?php
use yii\helpers\Html;
use yii\helpers\Url;
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div id="table-1">
<table class ="table">
  <tr>
    <th>ID</th>
    <th>Tên </th>
    <th>email</th>
    <th>Địa chỉ</th>
    <th>Số điện thoại</th>
    <th></th>
  </tr>
  <?php foreach ($model as $value) {?>
  <tr class ="table-content">
    <td><?php echo ($value['id_user']); ?></td>
    <td><?php echo ($value['user_name']); ?></td>
    <td><?php echo ($value['email_user']); ?></td>
    <td><?php echo ($value['user_address']); ?></td>
    <td><?php echo ($value['phone']); ?></td>
    <td>
        <a class="delete" href ="<?= Url::toRoute(['user/delete', 'id_user' => " {$value['id_user']} "]) ?>">Xóa</a>
    </td>
  </tr>
  <?php } ?>
</table>
</div>
</body>
</html>