<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);
$jsFile = '../assets/password.js';

// Nhúng file JavaScript
$this->registerJsFile($jsFile);

/* @var $this yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
</head>
<body>
<?php $this->beginBody() ?>
    <header>
      <div id="header-admin"> 
            <div class= "home-admin"><a href ="<?= Url::toRoute(['admin/index']) ?>"> Home </a> </div>
            <div class = 'account'>
                <?php if(isset($_SESSION['id_admin']) && isset($_SESSION['name_admin'])) { ?>
                    <div><a href ="<?= Url::toRoute(['admin/logout']) ?>">Logout : <?php echo "xin chào:".$_SESSION['name_admin'] ?> </a> </div>
                <?php } else { ?>
                    <div class = "login-admin"><a href ="<?= Url::toRoute(['admin/login']) ?>">Login </a></div> 
                    <div> &#x7C; </div>
                    <div class = "sigip-admin"><a href ="<?= Url::toRoute(['admin/register']) ?>"> sigup </a></div>
                    <?php } ?>
            </div>
      </div>
    </header>
    <main role="main" class="flex-shrink-0">
    <div class="container" >
        <div style = "display: block; padding: 10px; border: 1px solid black;border-radius: 20px;
    background-color: #b834bc ;min-width: 910px;">
            <div id= "menu">
                <div class ='item'>
                    <a href ="<?= Url::toRoute(['product/index']) ?>">Quản lý sản phẩm</a>
                </div>
                <div class ='item'>
                    <a href ="<?= Url::toRoute(['product/size']) ?>">Quản lý kích thước sản phẩm</a>
                </div>
                <div class ='item'>
                    <a href ="<?= Url::toRoute(['user/index']) ?>">Quản lý tài khoản User</a>
                </div>
                <div class ='item'>
                    <a href ="<?= Url::toRoute(['product/categorys']) ?>">Quản lý danh mục sản phẩm</a>
                </div>
                <div class ='item'>
                    <a href ="<?= Url::toRoute(['admin/order']) ?>">Quản lý danh sách đơn hàng</a>
                </div>
                <div class ='item'>
                    <a href ="<?= Url::toRoute(['reviews/index']) ?>">reviews sản phẩm</a>
                </div>
            </div>
        </div>
        <?= $content ?>
    </div>
</main>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
