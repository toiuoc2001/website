<?php
use yii\helpers\Html;
use yii\helpers\Url;
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div id="table-1">
    <div>
        <!-- <a class ="add-product" href ="<?= Url::toRoute(['product/create']) ?>"> Thêm sản phẩm </a> -->
    </div>
<table class ="table">
  <tr>
    <th style = "border: 1px solid;">ID</th>
    <th style = "border: 1px solid;">Tên người đặt hàng</th>
    <th style = "border: 1px solid;">số điện thoại</th>
    <th style = "border: 1px solid;">Tổng số sản phẩm</th>
    <th style = "border: 1px solid;">Tổng tiền</th>
    <th style = "border: 1px solid;">trạng thái</th>
    <th></th>
  </tr>
  <?php foreach ($data as $value) {?>
  <tr class ="table-content">
    <td><?php echo ($value['id_order_detail']); ?></td>
    <td><?php echo ($value['user_name']); ?></td>
    <td><?php echo ($value['phone']); ?></td>
    <td><?php echo ($value['quantity']); ?></td>
    <td><?php echo ($value['total_price']); ?></td>
    <?php if ($value['status'] == 1) { ?>
        <td>đang chuyển</td>
        <?php } else { ?>
            <td>chưa chuyển</td>
            <?php }?>
    <td style = "display: flex; flex-direction: column">
        <a class ="edit" href ="<?= Url::toRoute(['admin/order-detail', 'id_order_detail' => " {$value['id_order_detail']} "]) ?>">chi tiết</a>
        <a class="delete" href ="<?= Url::toRoute(['admin/order-confirmation', 'id_order_detail' => " {$value['id_order_detail']} "]) ?>">xác nhận đơn</a>
        <a class="delete" href ="<?= Url::toRoute(['admin/delete-order', 'id_order_detail' => " {$value['id_order_detail']} "]) ?>">Xóa</a>
    </td>
  </tr>
  <?php } ?>
</table>
</div>
</body>
</html>