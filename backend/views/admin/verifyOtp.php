<!-- xác minh OTP -->
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<h2>Xác minh OTP</h2>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'otp_code')->input('text', ['autofocus' => true]) ?>

<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end() ?>
<a href = "<?= Url::to(['admin/send-otp', 'id_admin' => $_GET['id_admin']])?>"> Gửi lại mã xác nhận </a>