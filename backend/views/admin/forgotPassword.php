<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h2>Vui lòng nhập email </h2>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'email')->input('text', ['autofocus' => true]) ?>

<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>