<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<!-- đổi mật khẩu  -->
<h2>đổi mật khẩu</h2>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'password')->input('password', ['autofocus' => true , 'id' => 'password']) ?>

<div class = "error-message"></div>
<?= $form->field($model, 'password_2')->input('password', ['autofocus' => true, 'id' => 'password_2']) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end() ?>
<script>
    // làm tính năng nếu mật không giống nhau hiển thị thông báo lỗi cho người dùng
    var password = document.getElementById("password");
    var password_2 = document.getElementById("password_2");
    var message = document.querySelector(".error-message");
    password_2.addEventListener('input', function(){
        if (password.value == password_2.value) {
            message.innerText = "";
        } else {
            message.innerText = "mật không khởp nhau";
        }
    })
</script>