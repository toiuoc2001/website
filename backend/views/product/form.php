<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$category = ArrayHelper::map($categorys,'id_product_categorys', 'categorys_name')
?>

<a style = "font-size: 30px; font-weight: 800;" href="<?= Url::toRoute(['index']) ?>" > Trang chủ </a>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<div style = "background-color: #d1e0e0; display: flex; justify-content: center; align-items: center; height: auto;  flex-direction: column; border-radius: 20px;">
    <div>
        <b style = "font-size: 26px;"> Thêm sản phẩm </b>
    </div>

    <div class = "input">
    <label >Tên sản phẩm</label>
    <?= $form->field($model, 'product_name')->input('text') ?>
    </div>

    <div class = "input">
    <label >danh mục sản phẩm</label>
    <?= $form->field($model, 'id_product_categorys')->dropDownList($category) ?>
    </div>
    
    <div class = "input">
    <label >hình ảnh</label>
    <?= $form->field($model, 'product_image')->input('file') ?>
    </div>

    <div style = "display: flex;">
        <div class= "input" style = "margin-right: 10px; ">
        <label >Giá sản phảm</label>
        <?=  $form->field($model, 'product_price') -> input('text') ?>
        </div>
        <div class = "input" style = "margin-left: 10px; ">
        <label >Số lượng</label>
        <?= $form->field($model, 'quantity') -> input('text') ?>
        </div>
    </div>
    
    <div class = "input">
    <label >Thông tin giới thiệu sản phẩm</label>
    <?= $form->field($model, 'product_info') -> textarea(['row' => 6]) ?>
    </div>

    <div style = "margin-top: 10px; margin-bottom: 10px;">
        <button class= "btn btn-primary" style ="width: 300px; border-radius: 20px; max-height: 100%; "> Lưu </button>
    </div>
</div>
<?php ActiveForm::end() ?>

<style> 
.input {
    /* //border-bottom: 1px solid; */
    max-width: 435px;
    width: 100%;
    height: 100%;
}
.input .control-label {
    width: 20px; /* Chọn một giá trị phù hợp với nội dung của label */
}
.input label {
    width: 100%; /* Đảm bảo label chiếm toàn bộ độ rộng của input */
}
</style>
