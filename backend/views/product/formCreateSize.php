<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin() ?>
<?= $form->field($model, 'name_size')->input('text') ?>
<?= Html::submitButton('thêm', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>