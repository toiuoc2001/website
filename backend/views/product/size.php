<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div id="table-1">
    <div style ="display: flex">
        <div>
            <a  class ="add-product" href ="<?= Url::toRoute(['product/create-size']) ?>"> Thêm kích thức</a>
        </div>
        <div>
            <a style ="margin-left: 10px" class ="add-product" href ="<?= Url::toRoute(['product/add-size']) ?>"> Thêm kích thức của sản phẩm</a>
        </div>
    </div>
  <table class ="table">
    <tr>
      <th>ID</th>
      <th>kích thức</th>
      <th></th>
    </tr>
    <?php foreach ($model as $value) {?>
    <tr class ="table-content">
      <td><?php echo $value['id_size'] ?></td>
      <td><?php echo $value['name_size'] ?></td>
      <td>
          <a class ="edit" href ="<?= Url::toRoute(['product/update-size', 'id_size' => "{$value['id_size']}"]) ?>">Sửa</a>
          <a class="delete" href ="<?= Url::toRoute(['product/delete-size','id_size' => "{$value['id_size']}"]) ?>">Xóa</a>
      </td>
    </tr>
    <?php } ?>
  </table>
  <!-- hiển thị danh sách sản phẩm đã được thêm kích thước -->
  <h2>Danh sách các sản phẩm </h2>
  <table class ="table">
    <tr>
      <th>Tên sản phẩm</th>
      <th>hình ảnh</th>
      <th>kích thức</th>
      <th></th>
    </tr>
    <?php foreach ($result as $row) {?>
    <tr class ="table-content">
      <td><?php echo $row['product_name'] ?></td>
      <td><img class ="image" src ="<?php echo $row['product_image'] ?>" ></td>
      <td><?php echo $row['name_size'] ?></td>
      <td>
          <a class ="edit" href ="<?= Url::toRoute(['product/update-product-size', 'id_product_size' => "{$row['id_product_size']}"]) ?>">Sửa</a>
          <a class="delete" href ="<?= Url::toRoute(['product/delete-product-size','id_product_size' => "{$row['id_product_size']}"]) ?>">Xóa</a>
      </td>
    </tr>
    <?php } ?>
  </table>
</div>
</body>
</html>