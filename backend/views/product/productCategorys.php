<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div id="table-1">
    <div style ="display: flex">
        <div>
            <a  class ="add-product" href ="<?= Url::toRoute(['product/create-categorys']) ?>"> Thêm danh mục sản phẩm </a>
        </div>
    </div>
  <table class ="table">
    <tr>
      <th>ID</th>
      <th>Danh mục</th>
      <th></th>
    </tr>
    <?php foreach ($model as $value) {?>
    <tr class ="table-content">
      <td><?php echo $value['id_product_categorys'] ?></td>
      <td><?php echo $value['categorys_name'] ?></td>
      <td>
          <a class ="edit" href ="<?= Url::toRoute(['product/update-categorys', 'id_product_categorys' => "{$value['id_product_categorys']}"]) ?>">Sửa</a>
          <a class="delete" href ="<?= Url::toRoute(['product/delete-categorys','id_product_categorys' => "{$value['id_product_categorys']}"]) ?>">Xóa</a>
      </td>
    </tr>
    <?php } ?>
  </table>
  <!-- hiển thị danh sách sản phẩm đã được thêm kích thước -->
  <h2>Danh sách các sản phẩm </h2>
</div>
</body>
</html>