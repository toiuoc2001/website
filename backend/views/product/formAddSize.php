<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$productS = ArrayHelper::map($product, 'id_product', 'product_name');
$sizeA = ArrayHelper::map($size, 'id_size','name_size'); 
?>
<?php $form =ActiveForm::begin() ?>

<?= $form->field($model,'id_product')->dropDownList($productS) ?>

<?= $form->field($model,'id_size')->dropDownList($sizeA) ?>

<button type ='submit'> Thêm </button>
<?php ActiveForm::end() ?>