<?php
use yii\helpers\Html;
use yii\helpers\Url;
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div id="table-1">
    <div>
        <a class ="add-product" href ="<?= Url::toRoute(['product/create']) ?>"> Thêm sản phẩm </a>
    </div>
<table class ="table">
  <tr>
    <th style = "border: 1px solid;">ID</th>
    <th style = "border: 1px solid;">Tên sản phẩm</th>
    <th style = "border: 1px solid;">Tên danh mục sản phẩm</th>
    <th style = "border: 1px solid;">Hình ảnh sản phẩm</th>
    <th style = "border: 1px solid;">số lượng sản phẩm</th>
    <th style = "border: 1px solid;">kích thước</th>
    <th style = "border: 1px solid;">giá sản phẩm trên 1</th>
    <th></th>
  </tr>
  <?php foreach ($model as $value) {?>
  <tr class ="table-content">
    <td><?php echo ($value['id_product']); ?></td>
    <td><?php echo ($value['product_name']); ?></td>
    <td><?php echo ($value['categorys_name']); ?></td>
    <td><img class ="image" src ="<?php echo "../imgs/".$value['product_image'] ?>" alt ="<?php echo $value['product_image'] ?>"></td>
    <!-- <td><img class="image" src="<?= Yii::$app->urlManager->baseUrl ?>/assets/image/<?= $value['product_image'] ?>" ></td> -->
    <td><?php echo ($value['quantity']); ?></td>
    <td>2</td>
    <td><?php echo ($value['product_price']); ?></td>
    <td>
        <a class ="edit" href ="<?= Url::toRoute(['product/update', 'id_product' => " {$value['id_product']} "]) ?>">Sửa</a>
        <a class="delete" href ="<?= Url::toRoute(['product/delete', 'id_product' => " {$value['id_product']} "]) ?>">Xóa</a>
    </td>
  </tr>
  <?php } ?>
</table>
</div>
</body>
</html>
