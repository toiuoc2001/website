<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ProductReviewsSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="product-reviews-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'reviews_id') ?>

    <?= $form->field($model, 'id_product') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'rating') ?>

    <?= $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'review_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
