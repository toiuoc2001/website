<?php

use backend\models\ProductReviews;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\ProductReviewsSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Danh sách đánh giá sản phẩm';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-reviews-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p>
        <?= Html::a('Create Product Reviews', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'reviews_id',
            'id_product',
            'id_user',
            'rating',
            'comment',
            'review_date',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ProductReviews $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'reviews_id' => $model->reviews_id]);
                 }
            ],
        ],
    ]); ?>


</div>