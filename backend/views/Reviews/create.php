<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ProductReviews $model */

$this->title = 'Create Product Reviews';
$this->params['breadcrumbs'][] = ['label' => 'Product Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-reviews-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
