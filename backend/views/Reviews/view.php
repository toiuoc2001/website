<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ProductReviews $model */

$this->title = $model->reviews_id;
$this->params['breadcrumbs'][] = ['label' => 'Product Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-reviews-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'reviews_id' => $model->reviews_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'reviews_id' => $model->reviews_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'reviews_id',
            'id_product',
            'id_user',
            'rating',
            'comment',
            'review_date',
        ],
    ]) ?>

</div>
