<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ProductReviews $model */

$this->title = 'Update Product Reviews: ' . $model->reviews_id;
$this->params['breadcrumbs'][] = ['label' => 'Product Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reviews_id, 'url' => ['view', 'reviews_id' => $model->reviews_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-reviews-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
